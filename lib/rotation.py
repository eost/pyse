# -*- coding: utf-8 -*-
"""
Rotation routine, converting standard data (XYZ) to rotated data (UVW)
according to a given matrix available in _matrices.py

:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from obspy.core import Stream
import numpy as np

from pyse_util import verify_stream


def rotate(stream, matrix, change_name=True):
    """
    Function performing a simple rotation of a stream according a given matrix.

    :param stream: Stream object from module obspy.core.stream containing \
    3 traces.
    """
    # verify and sort stream
    if len(stream) == 3:
        verify_stream(stream)
    else:
        print("[rotation.rotate]: Stream not containing 3 traces")
        raise SystemExit

    # verify rotation matrix
    if matrix.shape != (3, 3):
        print("[rotation.rotate]: Rotation matrix has a bad shape")
        raise SystemExit

    # building data matrix
    trace_1 = stream[0]
    trace_2 = stream[1]
    trace_3 = stream[2]
    data = np.array([trace_1.data, trace_2.data, trace_3.data])

    # dot product
    t = np.dot(matrix, data)

    # new traces preparation
    trace_a = trace_1.copy()
    trace_b = trace_2.copy()
    trace_c = trace_3.copy()

    # feeding new traces data
    trace_a.data = t[0]
    trace_b.data = t[1]
    trace_c.data = t[2]

    if change_name:
        # modify channel names
        if trace_a.stats.channel[-1] == 'E':
            trace_a.stats.channel = trace_a.stats.channel.replace('E', 'U')
            trace_b.stats.channel = trace_b.stats.channel.replace('N', 'V')
            trace_c.stats.channel = trace_c.stats.channel.replace('Z', 'W')

        elif trace_a.stats.channel[-1] == 'U':
            trace_a.stats.channel = trace_a.stats.channel.replace('U', 'E')
            trace_b.stats.channel = trace_a.stats.channel.replace('V', 'N')
            trace_c.stats.channel = trace_a.stats.channel.replace('W', 'Z')

    return Stream(traces=[trace_a, trace_b, trace_c])


def xyz2uvw(streamXYZ, matrix):
    """
    Function performing a rotation of a XYZ data stream to a UVW data stream, \
    according a defined matrix in _matrices.py.

    :param streamXYZ: Stream object from module obspy.core.stream containing \
    data from a seismometer in XYZ mode.
    :type matrix: array
    :param matrix: Matrix defined in _matrices.py. It specifies a rotation \
    from XYZ to UVW.
    :returns: Stream object containing rotated data in UVW mode.
    """
    return rotate(streamXYZ, matrix)


def uvw2xyz(streamUVW, matrix):
    """
    Function performing a rotation of a UVW data stream to a XYZ data stream, \
    according the transpose of a defined matrix in _matrices.py.

    :param streamUVW: Stream object from module obspy.core.stream containing \
    data from a seismometer in UVW mode.
    :type matrix: array
    :param matrix: Matrix defined in _matrices.py. It specifies a rotation \
    from XYZ to UVW, and the program transposes it before rotation.
    :returns: Stream object containing rotated data in XYZ mode.
    """
    return rotate(streamUVW, matrix.transpose())
