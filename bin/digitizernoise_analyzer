#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

 Usage:
    digitizernoise_analyzer fdsn <start> <end> <stream> [--url=<url>] \
[--sensitivity=<sens>]
    digitizernoise_analyzer mseed <file> [--sensitivity=<sens>]

Example:
    digitizernoise_analyzer fdsn 2017-01-16T15:00:00 2017-01-16T16:00:00 \
XX.GP003.00.HH?

Options:
    -h --help           Show this screen.
    --version           Show version.
    <start>             Start time in obspy.core.UTCDateTime format.
    <end>               End time in obspy.core.UTCDateTime format.
    <stream>            Stream to analyse in seed code separated by dots. \
Multiple streams are allowed, separated by comma \
(Ex: XX.GPIL.?0.HHZ,FR.STR.00.HHZ). Only 3 traces are allowed.
    --url=<url>         Set base url of fdsnws server, fdsn mode only. \
[default: http://dataselect.u-strasbg.fr:8080].
    <file>              Mseed file to read, mseed mode only.
    --sensitivity=<sens>    Set sensitivity [default: 400000].
"""
from docopt import docopt
from matplotlib import pyplot
import numpy as np
from obspy.clients.fdsn import Client as fClient
from obspy.core import UTCDateTime, Stream, read

from pyse_util import calc_custom_psd


if __name__ == '__main__':
    args = docopt(__doc__, version='digitizernoise_analyzer 1.0')
    # Uncomment for debug
    # print(args)

    if args['fdsn']:
        data = fClient(base_url=args['--url'],
                       service_mappings={'event': None})
        t1 = UTCDateTime(args['<start>'])
        t2 = UTCDateTime(args['<end>'])
        streams = args['<stream>'].split(',')
        st = Stream()
        for stream in streams:
            code = stream.split('.')
            st += data.get_waveforms(code[0], code[1], code[2], code[3], t1,
                                     t2)
        st.trim(t1, t2)

    elif args['mseed']:
        st = read(args['<file>'])

    st.sort()
    print(st)
    st.plot(equal_scale=False)

    for tr in st:
        P, f = calc_custom_psd(tr, n_fft=1024)
        P /= float(args['--sensitivity'])**2
        pyplot.semilogx(1./f, 10*np.log10(P), label=tr.id)

    pyplot.grid()
    pyplot.xlabel("Period (s)")
    pyplot.ylabel("Noise PSD (dB rel to 1V**2*Hz**-1)")
    pyplot.legend(loc='best')
    pyplot.title("Instrumental Noise")
    pyplot.show()
