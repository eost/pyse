/*
Sketch Arduino pour étalonner les sismomètres
A utiliser sur Arduino Uno avec Ethernet Shield et CTEW1 Shield développé à l'EOST
maj le 24/05/2013
*/

#include <CTEW1.h>

#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet2.h> 

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {  
  0x90, 0xA2, 0xDA, 0x10, 0x10, 0xCA };

IPAddress ip(192, 168, 1, 11);
IPAddress gateway(192, 168, 1, 1);
IPAddress netmask(255, 255, 255, 0);

unsigned int localPort = 58888;      // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //buffer to hold incoming packet,

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

//Instance de table vibrante
CTEW1 table;

void setup() {
  // start the Ethernet and UDP:
  Ethernet.begin(mac, ip, dns, gateway);
  Udp.begin(localPort);
    
  table.init();
}

void loop() {
   
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  IPAddress remote;
  
  String cmd;
  
  if(packetSize)
  {
    // read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    
    remote=Udp.remoteIP();    
    
    cmd=packetBuffer;
          
    if (cmd.substring(0, 6) == "Step"){
      table.step();
      
      // send a reply, to the IP address and port that sent us the packet we received
      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
      Udp.write("Done");
      Udp.endPacket();
    }
    
    if (cmd.substring(0, 6) == "Peak"){
      table.peakMode();
         
      // send a reply, to the IP address and port that sent us the packet we received
      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
      Udp.write("Done");
      Udp.endPacket();
    }
    
    if (cmd.substring(0, 6) == "Center"){
      table.centerMode();
      
      // send a reply, to the IP address and port that sent us the packet we received
      Udp.beginPacket(remote, localPort);
      Udp.write("Done");
      Udp.endPacket();
    }    
  }
}


