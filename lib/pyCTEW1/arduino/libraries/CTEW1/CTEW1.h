/* Arduino CT-EW1 librairie
 * Ecrit par Maxime Bès de Berc
 * 25 septembre 2012
 *
 * Cette librairie est utilisée avec la carte de contrôle CT-EW1/Arduino développée par l'EOST
 *
*/

#ifndef CTEW1_h
#define CTEW1_h

#include <Arduino.h>

//Assignation des sorties numériques de commande de la table
#define STEP_PIN	6
#define MODE_PIN	7

class CTEW1
{
	public:
		//Constructeur
		CTEW1();

		void init(void);

		/*
		* Description: 
		*	Envoie la commande step à la table (bouton poussoir sur commande manuelle)
		*/
		void step(void);

		/*
		* Description: 
		*	Active le mode "Center" de la table
		*/
		void centerMode(void);

		/*
		* Description: 
		*	Active le mode "peak to peak" de la table
		*/
		void peakMode(void);	
};

#endif
