monitoring
==========

Set of munin plugins for monitoring seismic stations.

Munin plugins
-------------

There is 3 things to do to use a plugin.

1. Create a symbolic link in the `/etc/munin/plugins` folder. You will need to
specify the ip address and eventually the port in the link name.

    ```bash
    ln -s /path/to/q330_ /etc/munin/plugins/q330_130.89.11.14
    ```

    or:

    ```bash
    ln -s /path/to/q330_ /etc/munin/plugins/q330_130.89.11.14:5331
    ```

2. Add a block in `/etc/munin/munin.conf` with the ip of the munin-node:

   ```
   [stations;FR.STR]
       address 127.0.0.1
   ```

3. Add a block in `/etc/munin/plugin-conf.d/munin-node`:

   ```
   [q330_130.89.11.14]
   host_name FR.STR
   ```

That's all!

### Change warning and critical thresholds

Warning and critical thresholds can be modified in the
`/etc/munin/plugin-conf.d/munin-node` file. Just add a block for your plugin
with the environnment variables you want to modifie:

```bash
[q330_*]
env.mass.warning -15:15
env.mass.critical -35:35
env.clock_quality.warning 80:
env.clock_quality.critical 60:
env.main_current.warning 200
env.main_current.critical 500
env.gps_antenna_current.warning 20
env.gps_antenna_current.critical 25
env.dp_memory.warning 5
env.dp_memory.critical 80
env.voltage.warning 12:
env.voltage.critical 11.5:
env.satellites_used.warning 4:
env.satellites_used.critical 2:
env.temperature.warning 45
env.temperature.critical 50
env.uptime.warning 7:
env.uptime.critical 1:

[taurus_*]
env.current.warning 200
env.current.critical 500
env.dop.warning 5
env.dop.critical 20
env.input_voltage.warning 12:
env.input_voltage.critical 11.5:
env.mass.warning -20:20
env.mass.critical -50:50
env.power.warning 2.5
env.power.critical 4
env.satellites_used.warning 4:
env.satellites_used.critical 2:
env.temperature.warning 45
env.temperature.critical 50
env.timing.warning 1000
env.timing.critical 5000
```
