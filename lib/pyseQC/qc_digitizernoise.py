#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np
from obspy.core import UTCDateTime

from pyse_util import calc_custom_psd


def _qc_digitizernoise(digitizernoise_requests, server, conf):

    if len(digitizernoise_requests) > 0:
        d_noise = list()

        i = 0
        for request in digitizernoise_requests:
            print("%d %s" % (i, request))
            i += 1
            words = request.split(' ')
            t1 = UTCDateTime(words[2])
            t2 = UTCDateTime(words[3])
            seed_codes = words[4].replace(words[4][-3:-1], 'HH').split('.')
            try:
                st = server.get_waveforms(seed_codes[0], seed_codes[1],
                                          seed_codes[2], seed_codes[3], t1, t2)
                st.trim(t1, t2)

                for tr in st:
                    (P, f) = calc_custom_psd(tr, n_fft=1024)
                    P /= float(conf['main']['digitizer_gain'])**2
                    d_noise.append(P)

            except Exception as e:
                print(e)

        if len(d_noise) > 0:

            d_noise = np.array(d_noise)

            fd = pyplot.figure('digitizers self-noise', dpi=150)
            ax = fd.gca()
            ax.semilogx(1./f, 10*np.log10(np.median(d_noise, axis=0)), 'b',
                        label='Median')
            ax.semilogx(1./f, 10*np.log10(d_noise.min(0)), 'b', alpha=0.4,
                        label='Min/Max')
            ax.semilogx(1./f, 10*np.log10(d_noise.max(0)), 'b', alpha=0.4)
            ax.grid()
            ax.set_ylabel("PSD (dB rel. to 1V**2/Hz)")
            ax.set_xlabel("Period (s)")
            fd.suptitle("Digitizers self-noise")
            ax.legend(loc='best')

            return fd
