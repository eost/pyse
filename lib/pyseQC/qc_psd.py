#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np
from obspy.core import Stream, UTCDateTime

from pyse_util import calc_custom_psd, deconv_vel_psd, make_vel_paz, \
    parse_paz_file, resample_traces, verify_stream


def _qc_psd(psd_requests, server, conf):

    if len(psd_requests) > 0:
        Z_psd_ratio = list()
        Z_gain = list()
        N_psd_ratio = list()
        N_gain = list()
        E_psd_ratio = list()
        E_gain = list()

        i = 0
        for request in psd_requests:
            print("%d %s" % (i, request))
            i += 1
            words = request.split(' ')
            t1 = UTCDateTime(words[2])
            t2 = UTCDateTime(words[3])
            streams = words[4].split(',')

            if 'paz_file' in conf['main'].keys():
                _paz = parse_paz_file(conf['main']['paz_file'])
            else:
                _paz = make_vel_paz(float(conf['main']['sensor_period']),
                                    float(conf['main']['sensor_damping']),
                                    float(conf['main']['sensor_gain']) *
                                    float(conf['main']['digitizer_gain']),
                                    float(conf['main']
                                              ['normalization_frequency']))

            for stream in streams:
                try:
                    seed_codes = stream.replace('BH', 'HH').split('.')
                    if seed_codes[0] == 'XX':
                        st = server.get_waveforms(seed_codes[0],
                                                  seed_codes[1],
                                                  seed_codes[2],
                                                  seed_codes[3], t1, t2)
                        st.trim(t1, t2)

                        for tr in st:
                            ref_stream = None
                            for word in words:
                                if word[:12] == '--ref-stream':
                                    ref_stream = word.split('=')[1]
                            if not ref_stream:
                                trr = server.get_waveforms('FR', 'STR', '00',
                                                           tr.stats.channel,
                                                           t1, t2)[0]
                            else:
                                code = ref_stream.split('.')
                                channel = code[3].replace(code[3][-1],
                                                          tr.stats.channel[-1])
                                trr = server.get_waveforms(code[0], code[1],
                                                           code[2], channel,
                                                           t1, t2)[0]
                            trr.trim(t1, t2)
                            _net = trr.stats.network
                            _sta = trr.stats.station
                            _loc = trr.stats.location
                            _cha = trr.stats.channel
                            inv_ref = server.get_stations(network=_net,
                                                          station=_sta,
                                                          location=_loc,
                                                          channel=_cha,
                                                          starttime=t1,
                                                          level='response')
                            resp_ref = inv_ref.get_response(trr.id, t1)

                            resample_traces(tr, trr)
                            verify_stream(Stream(traces=[tr, trr]))

                            (P, f) = calc_custom_psd(tr, n_fft=131072)
                            P = deconv_vel_psd(f, P, _paz)
                            (Pr, fr) = calc_custom_psd(trr, n_fft=131072)
                            Pr = deconv_vel_psd(fr, Pr, resp_ref)

                            nfreq = _paz['normalization_frequency']
                            index = np.abs(f-nfreq).argmin()
                            sensitivity = np.sqrt(P[index] / Pr[index]) *\
                                _paz['sensitivity']

                            if tr.stats.channel[-1] == 'Z':
                                Z_psd_ratio.append(P/Pr)
                                Z_gain.append(sensitivity)
                            elif tr.stats.channel[-1] == 'N':
                                N_psd_ratio.append(P/Pr)
                                N_gain.append(sensitivity)
                            elif tr.stats.channel[-1] == 'E':
                                E_psd_ratio.append(P/Pr)
                                E_gain.append(sensitivity)

                except Exception as e:
                    print(e)

        if len(Z_psd_ratio) > 0:
            Z_psd_ratio = np.array(Z_psd_ratio)
            Z_gain = np.array(Z_gain)
            N_psd_ratio = np.array(N_psd_ratio)
            N_gain = np.array(N_gain)
            E_psd_ratio = np.array(E_psd_ratio)
            E_gain = np.array(E_gain)

            print("PSD ratios statistics:")
            print("Median sensitivity of Vert. component: %.1f Vs/m" %
                  (np.median(Z_gain)))
            print("Deviation of Vert. sensitivities: %.1f Vs/m"
                  % (Z_gain.std()))
            print("Max error from theory of Vert. sensitivies: %.3f percent" %
                  (np.abs(1 - Z_gain/_paz['sensitivity']).max()*100))
            print("Max error from median of Vert. sensitivies: %.3f percent" %
                  (np.abs(1 - Z_gain/np.median(Z_gain)).max()*100))

            fp = pyplot.figure('psd ratios', dpi=150)

            if len(N_psd_ratio) > 0 and len(E_psd_ratio) > 0:
                ax1 = fp.add_subplot(311)
            else:
                ax1 = fp.gca()
            ax1.grid()
            ax1.set_ylabel("Vert. comp.")
            ax1.semilogx(1./f, 10*np.log10(Z_psd_ratio.mean(0)), 'r')
            ax1.semilogx(1./f, 10*np.log10(Z_psd_ratio.max(0)), 'r',
                         alpha=0.4)
            ax1.semilogx(1./f, 10*np.log10(Z_psd_ratio.min(0)), 'r',
                         alpha=0.4)
            ax1.set_xlim((1e-2, 1e3))

            if len(N_psd_ratio) > 0 and len(E_psd_ratio) > 0:
                ax2 = fp.add_subplot(312)
                ax2.grid()
                ax2.set_ylabel("North comp.")
                ax2.semilogx(1./f, 10*np.log10(N_psd_ratio.mean(0)), 'g')
                ax2.semilogx(1./f, 10*np.log10(N_psd_ratio.max(0)), 'g',
                             alpha=0.4)
                ax2.semilogx(1./f, 10*np.log10(N_psd_ratio.min(0)), 'g',
                             alpha=0.4)
                ax2.set_xlim((1e-2, 1e3))

                ax3 = fp.add_subplot(313)
                ax3.grid()
                ax3.set_ylabel("East comp.")
                ax3.semilogx(1./f, 10*np.log10(E_psd_ratio.mean(0)), 'b')
                ax3.semilogx(1./f, 10*np.log10(E_psd_ratio.max(0)), 'b',
                             alpha=0.4)
                ax3.semilogx(1./f, 10*np.log10(E_psd_ratio.min(0)), 'b',
                             alpha=0.4)
                ax3.set_xlim((1e-2, 1e3))
                ax3.set_xlabel("Period (s)")

                print("Median sensitivity of East component: %.1f Vs/m"
                      % (np.median(E_gain)))
                print("Deviation of East sensitivities: %.1f Vs/m"
                      % (E_gain.std()))
                print("Max error from theory of East sensitivies: %.3f percent"
                      % (np.abs(1 - E_gain/_paz['sensitivity']).max()*100))
                print("Max error from median of East sensitivies: %.3f percent"
                      % (np.abs(1 - E_gain/np.median(E_gain)).max()*100))

                print("Median sensitivity of North component: %.1f Vs/m"
                      % (np.median(N_gain)))
                print("Deviation of North sensitivities: %.1f Vs/m"
                      % (N_gain.std()))
                print("Max error from theory of North sensitivies: %.3f percent"
                      % (np.abs(1 - N_gain/_paz['sensitivity']).max()*100))
                print("Max error from median of North sensitivies: %.3f percent"
                      % (np.abs(1 - N_gain/np.median(N_gain)).max()*100))
            else:
                ax1.set_xlabel("Period (s)")

            fp.suptitle("PSDs ratios between sensors and reference")
            print("")

            return fp
