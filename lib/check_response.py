# -*- coding: utf-8 -*-
"""
    Obspy Response object (obspy.core.inventory.response.Response)reviewing
    routine.
    The global response is reviewed by check_response.
    A specific stage of a response is reviewed by check_stage function, with
    more details.
    Those functions provide a plot for quick evaluation. Each frequency
    response is computed through evalresp wrapper of response object
    (get_evalresp_response_for_frequencies method of Response object).
    A response object can be retrieved with get_response method of either
    inventory (obspy.core.inventory.inventory.Inventory) or nrl object
    (obspy.clients.nrl.client.NRL).

:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from copy import deepcopy
from obspy.core.inventory.response import CoefficientsTypeResponseStage
from obspy.core.inventory.response import FIRResponseStage
from obspy.core.inventory.response import PolesZerosResponseStage
from obspy.core.inventory.response import InstrumentSensitivity, Response
from matplotlib import pyplot
import numpy as np


def _plot_fir(stage):
    """
    Creates specific fir plot for visual review.
    :type stage: FIRResponseStage classe or CoefficientsTypeResponseStage
    class, defined in obspy.core.inventory.response
    :param stage: Stage object to evaluate
    """
    n_fft = 512
    if isinstance(stage, FIRResponseStage):
        coeff = make_norm_coeff(stage.coefficients, stage.symmetry)
    elif isinstance(stage, CoefficientsTypeResponseStage):
        coeff = stage.numerator
    frequencies = np.fft.rfftfreq(n_fft,
                                  d=1./stage.decimation_input_sample_rate)
    w = 2*np.pi*frequencies
    h = np.fft.rfft(coeff, n=n_fft)
    natural_phase = np.angle(h)
    correction_phase = np.angle(np.abs(h) *
                                np.exp(-1j*stage.decimation_correction*w))

    fig = pyplot.figure(num="Stage #%d" % stage.stage_sequence_number, dpi=150)
    fig.add_subplot(311)
    fig.add_subplot(312)
    fig.add_subplot(313)
    fig.axes[0].plot(coeff)
    fig.axes[0].axvline(stage.decimation_delay *
                        stage.decimation_input_sample_rate, color='r',
                        label='Delay', alpha=0.5)
    fig.axes[0].legend(loc='best')
    fig.axes[0].set_ylabel("Coefficients")
    fig.axes[1].plot(frequencies, 20*np.log10(np.abs(h)))
    fig.axes[2].plot(frequencies, np.unwrap(natural_phase-correction_phase))
    for ax in fig.axes[1:]:
        ax.axvline(stage.decimation_input_sample_rate /
                   (2*stage.decimation_factor),
                   color='r', label='Nyquist freq.', alpha=0.5)
    fig.axes[1].legend(loc='best')
    fig.axes[1].set_ylabel("Amplitude [dB]")
    fig.axes[2].set_ylim(-1*np.pi, np.pi)
    fig.axes[2].set_ylabel("Phase [rad]")
    fig.axes[2].set_xlabel("Normalized Frequencies")
    fig.suptitle("Stage #%d: Digital FIR filter"
                 % stage.stage_sequence_number)
    [ax.grid() for ax in fig.axes]


def _plot_iir(stage):
    """
    Creates specific iir plot for visual review.
    :type stage: PolesZerosResponseStages (DECIMAL), defined in
    obspy.core.inventory.response
    :param stage: Stage object to evaluate
    """
    fig = pyplot.figure(num="Stage #%d" % stage.stage_sequence_number, dpi=150)
    frequencies = np.linspace(0, stage.decimation_input_sample_rate,
                              1000)
    h = get_resp_freq(stage, frequencies)
    fig.add_subplot(211)
    fig.add_subplot(212)
    fig.axes[0].plot(frequencies/stage.decimation_input_sample_rate,
                     20*np.log10(np.abs(h)))
    fig.axes[1].plot(frequencies/stage.decimation_input_sample_rate,
                     np.unwrap(np.angle(h)))
    for ax in fig.axes:
        ax.axvline(1./(2*stage.decimation_factor), color='r',
                   label='Nyquist freq.', alpha=0.5)
    fig.axes[0].legend(loc='best')
    fig.axes[0].set_ylabel("Amplitude [dB]")
    fig.axes[1].set_ylabel("Phase [rad]")
    fig.axes[1].set_xlabel("Normalized Frequencies")
    fig.suptitle("Stage #%d: Digital IIR filter"
                 % stage.stage_sequence_number)
    [ax.grid() for ax in fig.axes]


def _plot_analog(stage, sps):
    """
    Creates specific fir plot for visual review.
    :type stage: PolesZerosResponseStages (LAPLACE), defined in
    obspy.core.inventory.response
    :param stage: Stage object to evaluate
    """
    fig = pyplot.figure(num="Stage #%d" % stage.stage_sequence_number,
                        dpi=150)
    fig.add_subplot(211)
    fig.add_subplot(212)
    frequencies = np.geomspace(0.001, sps/2, num=10000)
    h = get_resp_freq(stage, frequencies)
    fig.axes[0].semilogx(frequencies, 20*np.log10(np.abs(h)), 'b')
    fig.axes[1].semilogx(frequencies, np.angle(h, deg=True), 'b')
    for ax in fig.axes:
        ax.axvline(stage.stage_gain_frequency, color='r',
                   label='Gain freq.', alpha=0.5)
        ax.axvline(stage.normalization_frequency, color='g',
                   label='Norm. freq.', alpha=0.5)
    fig.axes[0].legend(loc='best')
    fig.axes[0].set_ylabel("Amplitude [dB][%s/(%s)]"
                           % (stage.output_units, stage.input_units))
    fig.axes[1].set_ylabel("Phase [deg]")
    fig.axes[1].set_xlabel("Frequencies  [Hz]")
    fig.suptitle("Stage #%d: Analog filter"
                 % stage.stage_sequence_number)
    [ax.grid() for ax in fig.axes]


def check_fir_shape(stage):
    """
    Evaluates different parameter of FIR stage: sum, attenuation at Fe/2,
    cutoff frequency.
    :type stage: One of stage classes defined in obspy.core.inventory.response
    :param stage: Stage object to evaluate
    """
    if isinstance(stage, FIRResponseStage):
        coeff = make_norm_coeff(stage.coefficients, stage.symmetry)
        print("Symmetry: %s" % stage.symmetry)
    elif isinstance(stage, CoefficientsTypeResponseStage):
        coeff = np.array(stage.numerator)
    print("Number of coefficients: %d" % coeff.size)
    print("Coefficients sum: %.12f" % (np.sum(coeff)))
    frequencies = np.linspace(0, stage.decimation_input_sample_rate/2, 1000)
    h = get_resp_freq(stage, frequencies)
    frequencies /= stage.decimation_input_sample_rate
    iN = np.abs(frequencies-1./(2*stage.decimation_factor)).argmin()
    iC = np.abs(h*frequencies).argmax()
    print("Attenuation at Nyquist: %.1f dB"
          % (np.abs(20*np.log10(h[iN]))))
    print("Cutoff frequency: %.1f percent of Nyquist"
          % (frequencies[iC]*100/frequencies[iN]))
    print("Phase delay: %f" % stage.decimation_delay)
    print("Corrected delay: %f" % stage.decimation_correction)


def check_response(response):
    """
    Review a complete response by creating a plot containing the overall
    response and each response stage.
    :type stage: One of stage classes defined in obspy.core.inventory.response
    :param stage: Stage object to evaluate
    """
    fig = pyplot.figure(dpi=150)
    fig.axes[0] = fig.add_subplot(211)
    fig.axes[1] = fig.add_subplot(212)
    [ax.grid() for ax in fig.axes]
    last_stage = response.response_stages[-1]
    output_sampling_rate = (last_stage.decimation_input_sample_rate /
                            last_stage.decimation_factor)
    frequencies = np.geomspace(0.001, output_sampling_rate, 1000)
    H = np.ones(frequencies.size, dtype=np.complex128)
    for stage in response.response_stages:
        h = get_resp_freq(stage, frequencies)
        fig.axes[0].semilogx(frequencies, 20*np.log10(np.abs(h)), alpha=0.5,
                             label='stage #%d' % stage.stage_sequence_number)
        fig.axes[1].semilogx(frequencies, np.angle(h, deg=True), alpha=0.5)
        H *= h

    fig.axes[0].semilogx(frequencies, 20*np.log10(np.abs(H)), 'k')
    fig.axes[1].semilogx(frequencies, np.angle(H, deg=True), 'k')
    fig.axes[0].legend()
    fig.axes[1].set_ylim(-180, 180)


def check_stage(response, stage_sequence_number):
    """
    Review a specific stage.
    :type stage: One of stage classes defined in obspy.core.inventory.response
    :param stage: Stage object to evaluate
    """
    stage = response.response_stages[stage_sequence_number-1]
    print(stage)
    if isinstance(stage, PolesZerosResponseStage):
        if stage.pz_transfer_function_type[:7] == 'LAPLACE':
            # Analog filter
            last_stage = response.response_stages[-1]
            sps = (last_stage.decimation_input_sample_rate /
                   last_stage.decimation_factor)
            _plot_analog(stage, sps)
        else:
            # Digital IIR filter
            _plot_iir(stage)

    if isinstance(stage, CoefficientsTypeResponseStage):
        if (stage.cf_transfer_function_type == 'DIGITAL' and
           len(stage.numerator) > 1):
                if len(stage.denominator) == 0:
                    # Digital FIR filter
                    _plot_fir(stage)
                    check_fir_shape(stage)

    if isinstance(stage, FIRResponseStage):
        # Digital FIR filter
        _plot_fir(stage)
        check_fir_shape(stage)
    print()


def get_resp_freq(stage, frequencies):
    """
    Returns frequency response of a stage using evalresp.
    :type stage: One of stage classes defined in obspy.core.inventory.response
    :param stage: Stage object to evaluate
    :type frequencies: numpy.ndarray
    :param frequencies: Discrete frequencies to calculate response for.
    :rtype: numpy.ndarray
    :returns: Frequency response at requested frequencies
    """
    tmp_stage = deepcopy(stage)
    tmp_sens = InstrumentSensitivity(tmp_stage.stage_gain,
                                     tmp_stage.stage_gain_frequency,
                                     tmp_stage.input_units,
                                     tmp_stage.output_units)
    tmp_stage.stage_sequence_number = 1
    tmp_resp = Response(instrument_sensitivity=tmp_sens,
                        response_stages=[tmp_stage])
    h = tmp_resp.get_evalresp_response_for_frequencies(frequencies)
    return h


def make_norm_coeff(coefficients, symmetry):
    """
    Returns complete set of normalized coefficients. Used by FIR stages.
    :type coefficients: numpy.ndarray
    :param coefficients: Fir coefficients as found in stage
    :type symmetry: str
    :param symmetry: Type of symmetry, one of: 'ODD', 'EVEN', 'NONE'
    :rtype: numpy.ndarray
    :returns: Normalized coefficients
    """
    if symmetry == 'ODD':
        coeff = np.append(coefficients, coefficients[-2::-1])
    elif symmetry == 'EVEN':
        coeff = np.append(coefficients, coefficients[-1::-1])
    elif symmetry == 'NONE':
        coeff = np.array(coefficients)
    scale_factor = np.sum(coeff)
    return np.divide(coeff, scale_factor, dtype=np.float128)


if __name__ == "__main__":
    from obspy.clients.nrl import NRL
    nrl = NRL()
    s_keys = ['DTCC (manuafacturers of SmartSolo)',
              '5 Hz',
              'Rc=1850, Rs=430000']
    d_keys = ['DTCC (manufacturers of SmartSolo',
              'SmartSolo IGU-16',
              '36 dB (64)',
              '500',
              'Linear Phase',
              'Off']
    response = nrl.get_response(sensor_keys=s_keys, datalogger_keys=d_keys)
    for stage in response.response_stages:
        check_stage(response, stage.stage_sequence_number)
    check_response(response)
    pyplot.show()
