#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr) and Jean-Jacques Leveque \
    (leveque@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    step_injector -M <model> [-A <address>] [-P <port>] [-n <nStep>] [-d \
<duration>] [-t <trailer>] [-s <settling>]

Example:
    ./step_injector -M t120 -A 192.168.1.10 -P 58888 -n 50 -d 200 -s 120 -t 120

Options:
    -h --help           Show this screen.
    --version           Show version.
    -M <model>          Set model of seismometer [default: t120].
    -A <address>        Set Ip address of arduino [default: 192.168.1.10].
    -P <port>           Set UDP port used for arduino [default: 58888].
    -n <nStep>          Set number of steps requested [default: 3].
    -d <duration>       Set duration of each step in seconds [default: 200].
    -s <settling>       Set settling duration in seconds [default: 120].
    -t <trailer>        Set trailer duration in seconds [default: 120].
"""
from docopt import docopt
import time
from obspy.core import UTCDateTime

from pySismoCtrl import Sismo

if __name__ == '__main__':
    args = docopt(__doc__, version='step_injector 1.0')
    # Uncomment for debug
    # print(args)

    print(UTCDateTime.now())
    sismo = Sismo(args['-A'], int(args['-P']), args['-M'])
    sismo.uvw()
    print("Step injector online")
    time.sleep(int(args['-s']))
    for i in range(int(args['-n'])):
        sismo.injection()
        print("Cycle %s injection" % (str(i)))
        time.sleep(int(args['-d']))
        sismo.rest()
        print("Cycle %s rest" % (str(i)))
        time.sleep(int(args['-d']))
    time.sleep(int(args['-t']))
    sismo.xyz()
    sismo.close()
    print(UTCDateTime.now())
    print("Step injector offline")
