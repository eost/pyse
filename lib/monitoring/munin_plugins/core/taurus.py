# -*- coding: utf-8 -*-
"""
Q330 monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""

from datetime import timedelta
import logging
from lxml import etree
import os
import time
try:
    from urllib.error import URLError
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen, URLError


logger = logging.getLogger(__name__)


class TaurusThresholds:
    """
    Store Taurus warning and critical thresholds.
    """
    def __init__(self):
        self.current_warning = os.environ.get('current.warning') or 200
        self.current_critical = os.environ.get('current.critical') or 500
        self.dop_warning = os.environ.get('dop.warning') or 5
        self.dop_critical = os.environ.get('dop.critical') or 20
        self.voltage_warning = os.environ.get('voltage.warning') or '12:'
        self.voltage_critical = os.environ.get('voltage.critical') or '11.5:'
        self.mass_warning = os.environ.get('mass.warning') or '-20:20'
        self.mass_critical = os.environ.get('mass.critical') or '-50:50'
        self.power_warning = os.environ.get('power.warning') or 2.5
        self.power_critical = os.environ.get('power.critical') or 4
        self.satellites_used_warning = \
            os.environ.get('satellites_used.warning') or '4:'
        self.satellites_used_critical = \
            os.environ.get('satellites_used.critical') or '2:'
        self.temperature_warning = os.environ.get('temperature.warning') or 45
        self.temperature_critical = \
            os.environ.get('temperature.critical') or 50
        self.time_error_warning = \
            os.environ.get('time_error.warning') or 1000
        self.time_error_critical = \
            os.environ.get('time_error.critical') or 5000


class TaurusData:
    """
    Store Taurus status data
    """
    BYTES = {
        'B': 1,
        'KB': 1024,
        'MB': 1024**2,
        'GB': 1024**3,
        'TB': 1024**4,
    }

    SECONDS = {
        's': 1,
        'ms': 1e-3,
        'μs': 1e-6,
        'ns': 1e-9,
    }

    def __init__(self):
        self.sensor_current = 'U'
        self.digitizer_current = 'U'
        self.controller_current = 'U'
        self.dac_count = 'U'
        self.pdop = 'U'
        self.tdop = 'U'
        self.voltage = 'U'
        self.mass_1 = 'U'
        self.mass_2 = 'U'
        self.mass_3 = 'U'
        self.power = 'U'
        self.sensor_power = 'U'
        self.satellites_used = 'U'
        self.store_time_left = 'U'
        self.store_used = 'U'
        self.temperature = 'U'
        self.time_error = 'U'
        self.uncertainty = 'U'

    def _get_root(self, url):
        response = urlopen(url, timeout=15)
        parser = etree.HTMLParser()
        return etree.parse(response, parser)

    def _parse_int(self, root, path, factor=1, divisor=1):
        try:
            element = root.xpath(path)[0]
            return int(element.split(' ')[0]) * factor // divisor
        except (IndexError, TypeError, ValueError):
            return 'U'

    def _parse_float(self, root, path, factor=1, divisor=1):
        try:
            element = root.xpath(path)[0]
            return float(element.split(' ')[0]) * factor / divisor
        except (IndexError, TypeError, ValueError):
            return 'U'

    def _parse_time(self, root, path, factor=1, divisor=1):
        try:
            element = root.xpath(path)[0]
            value, unit = element.split()
            return float(value) * TaurusData.SECONDS[unit] * factor / divisor
        except (IndexError, TypeError, ValueError):
            return 'U'

    def _get_status_page_data(self, address, port):
        status_url = 'http://%s:%s/pages/taurus/status.page' % (address, port)
        try:
            root = self._get_root(status_url)
        except URLError as e:
            logger.error(e)
            return

        try:
            store_time_left_element = \
                root.xpath("//td/span[@id='lv_2']/text()")
            day_left, time_left = store_time_left_element[0].split('d')
            time_left = time.strptime(time_left.strip(), '%H:%M:%S')
            time_seconds = timedelta(hours=time_left.tm_hour,
                                     minutes=time_left.tm_min,
                                     seconds=time_left.tm_sec).total_seconds()
            self.store_time_left = int(day_left) + time_seconds / 86400
        except (IndexError, TypeError, ValueError):
            pass

        try:
            store_used_element = root.xpath("//td/span[@id='lv_3']/text()")[0]
            store_used, unit = store_used_element.split()
            store_used = float(store_used) * TaurusData.BYTES[unit]

            store_size_element = root.xpath("//td/span[@id='lv_4']/text()")[0]
            store_size, unit = store_size_element.split()
            store_size = float(store_size) * TaurusData.BYTES[unit]

            self.store_used = (store_used / store_size) * 100
        except (IndexError, TypeError, ValueError):
            pass

        self.power = \
            self._parse_float(root, "//td/span[@id='lv_8']/text()")

    def _get_soh_page_data(self, address, port):
        soh_url = 'http://%s:%s/pages/taurus/soh.page' % (address, port)
        try:
            root = self._get_root(soh_url)
        except URLError as e:
            logger.error(e)
            return

        self.temperature = \
            self._parse_float(root, "//td/span[@id='lv_0']/text()")
        self.voltage = \
            self._parse_float(root, "//td/span[@id='lv_1']/text()")
        self.sensor_current = \
            self._parse_float(root, "//td/span[@id='lv_2']/text()")
        self.digitizer_current = \
            self._parse_float(root, "//td/span[@id='lv_3']/text()")
        self.controller_current = \
            self._parse_float(root, "//td/span[@id='lv_4']/text()")

    def _get_timing_page_data(self, address, port):
        timing_url = \
            'http://%s:%s/pages/taurus/timingIndex.page' % (address, port)
        try:
            root = self._get_root(timing_url)
        except URLError as e:
            logger.error(e)
            return

        self.uncertainty = \
            self._parse_time(root, "//td/span[@id='lv_2']/text()", factor=1e9)
        self.time_error = \
            self._parse_time(root, "//td/span[@id='lv_3']/text()", factor=1e9)
        self.dac_count = self._parse_int(root, "//td/span[@id='lv_4']/text()")
        self.satellites_used = \
            self._parse_float(root, "//td/span[@id='lv_6']/text()")
        self.pdop = self._parse_float(root, "//td/span[@id='lv_7']/text()")
        self.tdop = self._parse_float(root, "//td/span[@id='lv_8']/text()")

    def _get_sensor_page_data(self, address, port):
        sensor_url = 'http://%s:%s/pages/taurus/sensor.page' % (address, port)
        try:
            root = self._get_root(sensor_url)
        except URLError as e:
            logger.error(e)
            return

        mass_1_path = "//td/b[text()='Mass 1:']/../following::td//td/text()"
        self.mass_1 = self._parse_float(root, mass_1_path)

        mass_2_path = "//td/b[text()='Mass 2:']/../following::td//td/text()"
        self.mass_2 = self._parse_float(root, mass_2_path)

        mass_3_path = "//td/b[text()='Mass 3:']/../following::td//td/text()"
        self.mass_3 = self._parse_float(root, mass_3_path)

        sensor_power_path = "//th[text()='Power:']/following::td//td/text()"
        self.sensor_power = self._parse_float(root, sensor_power_path)

    def import_data(self, address, port):
        """
        Return a Taurus object containing the monitored data.
        """
        self._get_status_page_data(address, port)
        self._get_soh_page_data(address, port)
        self._get_timing_page_data(address, port)
        self._get_sensor_page_data(address, port)
