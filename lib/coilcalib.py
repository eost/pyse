# -*- coding: utf-8 -*-
"""
:author:
    Hélène Jund (helene.jund@unistra.fr)
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Hélène Jund (helene.jund@unistra.fr)
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
import numpy as np
from matplotlib import mlab
from obspy.core.stream import Stream
from obspy.signal.invsim import cosine_taper
from obspy.signal.util import prev_pow_2

from pyse_util import basic_detrend_func, frac_octave_smooth


def H_psd_div(tr1, tr2, sm_n=0, fnorm=1, normalize=True):
    """
    Function returning amplitude and phase of a transfer function using a \
    power spectral density division between a reference signal (1) and the \
    response of a bb sensor and its calibration coil (2). \
    The calibration coil is a force transducer, and therefore applies an \
    acceleration. A bb sensor being a velocimeter, the reference signal must \
    be integrated.
    :type tr1: obspy.core.trace.Trace
    :param tr1: Trace containing the reference sensor injected in the coil.
    :type tr2: obspy.core.trace.Trace
    :param tr2: Response of broad-band sensor.
    :type sm_n: int
    :param sm_n: Smooth coefficient.
    :type fnorm: int
    :param fnorm: Normalization frequency.
    :type normalize: bool
    :param normalize: Flag to normalize amplitude or not (ie Amp(fnorm)=1)
    """
    # Transform given streams objects to traces
    if isinstance(tr1, Stream):
        if len(tr1) == 1:
            tr1 = tr1[0]
        else:
            msg = "Stream got multiple traces!"
            raise ValueError(msg)
    if isinstance(tr2, Stream):
        if len(tr2) == 1:
            tr2 = tr2[0]
        else:
            msg = "Stream got multiple traces!"
            raise ValueError(msg)

    # Check if sampling rate and trace length is the same
    if tr1.stats.npts != tr2.stats.npts:
        msg = "Traces don't have the same length!"
        raise ValueError(msg)
    elif tr1.stats.sampling_rate != tr2.stats.sampling_rate:
        msg = "Traces don't have the same sampling rate!"
        raise ValueError(msg)

    n_fft = prev_pow_2(int(tr1.stats.npts*1./13))
    n_overlap = int(n_fft*0.75)
    fs = tr1.stats.sampling_rate
    # Spectra calculation
    (P11, f) = mlab.psd(tr1.data, Fs=fs, NFFT=n_fft, noverlap=n_overlap,
                        detrend=basic_detrend_func,
                        window=cosine_taper(n_fft, p=0.2),
                        scale_by_freq=True)
    (P12, f) = mlab.csd(tr1.data, tr2.data, Fs=fs, NFFT=n_fft,
                        noverlap=n_overlap,
                        detrend=basic_detrend_func,
                        window=cosine_taper(n_fft, p=0.2),
                        scale_by_freq=True)

    # Division
    H = 2j*np.pi*f*P12/P11

    # Apply smoothing
    if sm_n != 0:
        H = frac_octave_smooth(sm_n, f, H)

    inorm = int(np.absolute(f-fnorm).argmin())
    P = np.angle(H, deg=True)

    # Check coil polarity with phase at norm. frequency
    if np.absolute(P[inorm]) > 100:
        H = -1. * H
        P = np.angle(H, deg=True)

    A0 = np.absolute(H[inorm])
    A = np.absolute(H)

    A = A[1:]
    P = P[1:]
    f = f[1:]

    if normalize:
        A = A/A0

    return (f, A, P)


def H_fft_div(tr1, tr2, sm_n=0, fnorm=1, normalize=True):
    """
    Function returning amplitude and phase of a transfer function using a \
    simple spectral division between a reference signal (1) and the response \
    of a bb sensor and its calibration coil (2) and a reference signal (2). \
    The calibration coil is a force transducer, and therefore applies an \
    acceleration. A bb sensor being a velocimeter, the reference signal must \
    be integrated.
    :type model: string
    :param model: String specifying the model of braod-band sensor.
    :type sm_coeff: int
    :param sm_coeff: Smooth coefficient.
    :type fnorm: int
    :param fnorm: Normalization frequency.
    :type normalize: bool
    :param normalize: Flag to normalize amplitude or not (ie Amp(fnorm)=1)
    """
    # Transform given streams objects to traces
    if isinstance(tr1, Stream):
        if len(tr1) == 1:
            tr1 = tr1[0]
        else:
            msg = "Stream got multiple traces!"
            raise ValueError(msg)
    if isinstance(tr2, Stream):
        if len(tr2) == 1:
            tr2 = tr2[0]
        else:
            msg = "Stream got multiple traces!"
            raise ValueError(msg)

    # Check if sampling rate and trace length is the same
    if tr1.stats.npts != tr2.stats.npts:
        msg = "Traces don't have the same length!"
        raise ValueError(msg)
    elif tr1.stats.sampling_rate != tr2.stats.sampling_rate:
        msg = "Traces don't have the same sampling rate!"
        raise ValueError(msg)

    # Spectra calculation
    spectre1 = np.fft.rfft(tr1.data)[1:]
    spectre2 = np.fft.rfft(tr2.data)[1:]
    f = np.fft.rfftfreq(tr1.stats.npts, d=tr1.stats.delta)[1:]

    # Division (2)/(1) with reference integration (ie 2j*pi*(2)/(1))
    H = 2j*np.pi*f*spectre2/spectre1

    # Apply smoothing
    if sm_n != 0:
        H = frac_octave_smooth(sm_n, f, H)

    inorm = int(np.absolute(f-fnorm).argmin())
    P = np.angle(H, deg=True)

    # Check coil polarity with phase at norm. frequency
    if np.absolute(P[inorm]) > 100:
        H = -1. * H
        P = np.angle(H, deg=True)

    A0 = np.absolute(H[inorm])
    A = np.absolute(H)

    if normalize:
        A = A/A0

    return (f, A, P)
