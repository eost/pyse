bbb_plugin Beaglebone/BeagleboneBlack ADCs acquisition plugin for seedlink
=======

Install
------------
Install and compile seiscomp3, see https://github.com/SeisComP3/seiscomp3 for sources.  
Install libpruio, see http://users.freebasic-portal.de/tjf/Projekte/libpruio/doc/html/_cha_preparation.html  
Compile program with  
	make

Running
------------
Like others seedlink plugin bbb_plugin can work on its own for test purposes  
	./bbb_plugin -f key_file 63>data.dat  
For running within seiscomp, you need to configure seedlink with streams.xml, filters.fir, and seedlink.ini.  
Examples files are provided, see http://www.seiscomp3.org/wiki/doc/trouble-shooting  

Key file
------------
The key file must contains at least 3 lines beginning by:  
	station=  
	mask=  
	rate=  
If those lines are not specified in key file, the program won't acquire anything and send an error.  
Station name must be at least 5 letters (according to Seed Manual), ex: station=TEST.  
Mask must be an hexadecimal number specified with 5 characters, see below for signification, ex: mask=0x1FE.  
Sampling rate must be specified with no more characters than 5, ie Fmax=99999Hz. BBB rev C dose not support more than 10000Hz, ex: rate=10000.  

Parameter mask must always be specified with 5 characters representing a hexadecimal number. It activates the chosen ADCs:   
    AIN0 = 0b000000010 = 0x002  
    AIN1 = 0b000000100 = 0x004  
    AIN2 = 0b000001000 = 0x008  
    AIN3 = 0b000010000 = 0x010  
    AIN4 = 0b000100000 = 0x020  
    AIN5 = 0b001000000 = 0x040  
    AIN6 = 0b010000000 = 0x080  
    AIN7 = 0b100000000 = 0x100  
Example: for activating the three first ADCs, mask = 0b000001110 = 0x00E  
A key file key_bbb.ini is provided as example.  

Contact
-------
Maxime Bès de Berc  
mbesdeberc@unistra.fr  

01 oct 2014
Updated 07 april 2015

