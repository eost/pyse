import csv
from matplotlib import pyplot
from multiprocessing.pool import ThreadPool
import numpy as np
from obspy import Stream, UTCDateTime
from obspy.clients.fdsn.client import Client
from obspy.clients.fdsn.header import FDSNNoDataException
from obspy.signal.util import next_pow_2
import os
import pandas as pd
from scipy.signal import csd
from scipy.special import factorial

from pyse_util import sort_stream
from rotation import rotate
import transmatrix as tm


class SelfnoiseEstimator:

    def __init__(self, fdsnws_url, stations_csv, outpath, start, end,
                 fmin, fmax, segment_length, overlap):
        self.fdsnws_url = fdsnws_url
        self.id_list = list()
        with open(stations_csv, 'r') as _file:
            idreader = csv.DictReader(_file, delimiter=',',
                                      quotechar='#')
            for row in idreader:
                self.id_list.append(row)
        self.start = start
        self.end = end
        self.fmin = fmin
        self.fmax = fmax
        self.segment_length = segment_length
        self.overlap = overlap
        self.outpath = outpath

    def _align_stream(self, st, st_ref):
        tmat = tm.transmatrix(st, st_ref, fmin=self.fmin, fmax=self.fmax)
        R, _r, _G = tm.matrix_2_tait_bryan_zyx(tmat)
        u_tmat = tm.tait_bryan_zyx_2_matrix(R[0], R[1], R[2])
        return rotate(st, np.linalg.inv(u_tmat), change_name=False)

    def _cmp_csd(self, st, i, j, nperseg, noverlap):
        fs = st[0].stats.sampling_rate
        _freq, _csd= csd(st[i].data, st[j].data, fs=fs, window='hann',
                        nperseg=nperseg, noverlap=noverlap,
                        nfft=next_pow_2(nperseg), detrend='constant',
                        return_onesided='True', scaling='density')
        return _freq, _csd

    def _cmp_selfnoise(self, i, csd_matrix, freq, tf):
        N = len(csd_matrix[0])
        Pii = csd_matrix[i][i]
        factor = 0.5 * np.divide(factorial(N - 3) * 2,
                                 factorial(N - 1))
        csd_sum = np.zeros(len(csd_matrix[i][i]),
                           dtype='complex128')
        for j in range(N):
            for k in range(N):
                if k != i and k != j and j != i:
                    csd_product = \
                        np.multiply(csd_matrix[j][i],
                                    csd_matrix[i][k])
                    csd_term = np.divide(csd_product,
                                         csd_matrix[j][k])
                    csd_sum += csd_term
        Nii = Pii - np.multiply(factor, csd_sum)
        Nii = np.divide(Nii, np.power(tf, 2))
        factor = np.power(np.multiply(2*np.pi, freq), 2)
        Nii = np.multiply(Nii, factor)
        return Nii

    def _get_stream(self, _id):
        try:
            _st = self.fdsnws.get_waveforms(_id['net'], _id['sta'],
                                            _id['loc'], _id['chan'],
                                            self.start, self.end,
                                            attach_response=True)
            _st.trim(self.start, self.end)
        except FDSNNoDataException:
            print("{}.{}.{}.{}: no data available".format(_id['net'],
                                                          _id['sta'],
                                                          _id['loc'],
                                                          _id['chan']))
        return sort_stream(_st, axis='nez')

    def align_streams(self):
        pool = Pool(os.cpu_count()-1)
        results = list()
        for _st in self.stream_list[1:]:
            _id = _st[0].id
            _comp = _id[-1]
            print("{}: being aligned".format(_id.replace(_comp, '?')))
            _re = pool.apply_async(self._align_stream,
                                   args=[_st, self.stream_list[0]])
            results.append(_re)
        pool.close()
        pool.join()
        aligned_stream_list = list()
        aligned_stream_list.append(self.stream_list[0])
        for _re in results:
            aligned_stream_list.append(_re.get())
        self.stream_list = aligned_stream_list


    def get_streams(self):
        self.fdsnws = Client(base_url=self.fdsnws_url)
        self.stream_list = list()
        pool = ThreadPool(os.cpu_count()-1)
        results = list()
        for _id in self.id_list:
            print("{}.{}.{}.{}: getting waveforms".format(_id['net'],
                                                          _id['sta'],
                                                          _id['loc'],
                                                          _id['chan']))
            _re = pool.apply_async(self._get_stream, args=[_id, ])
            results.append(_re)
        pool.close()
        pool.join()
        for _re in results:
            self.stream_list.append(_re.get())
        npts_list = list()
        for _st in self.stream_list:
            npts_list.extend([tr.stats.npts for tr in _st])
        if len(set(npts_list)) != 1:
            msg = "Length of the traces are not identical. One or more \
                   may be incomplete"
            raise Exception(msg)

    def process(self):
        for _comp in ['Z', 'N', 'E', '1', '2']:
            st = Stream()
            for _st in self.stream_list:
                st += _st.select(component=_comp)
            N = len(st)
            if N >= 3:
                resp = st[0].stats.response

                # Compute Cross Spectral Densities
                print("{} components: computing CSDs".format(_comp))
                nperseg = self.segment_length * \
                    st[0].stats.sampling_rate
                noverlap = nperseg * self.overlap
                results = [[0] * N for i in range(N)]
                pool = ThreadPool(os.cpu_count()-1)
                for i in range(N):
                    for j in range(N):
                        if i <= j:
                            _re = pool.apply_async(self._cmp_csd,
                                                   args=[st, i, j,
                                                         nperseg, noverlap])
                            results[i][j] = _re
                pool.close()
                pool.join()
                csd_matrix = [[0] * N for i in range(N)]
                for i in range(N):
                    for j in range(N):
                        if i <= j:
                            _re = results[i][j]
                            freq, _csd = _re.get()
                            csd_matrix[i][j] = _csd[1:]
                            if i != j:
                                csd_matrix[j][i] = np.conj(_csd[1:])
                freq = freq[1:]
                tf = resp.get_evalresp_response_for_frequencies(freq)

                # Compute each selfnoise
                results = list()
                pool = ThreadPool(os.cpu_count()-1)
                for i in range(N):
                    print("{}: computing self noise".format(st[i].id))
                    _re = pool.apply_async(self._cmp_selfnoise,
                                           args=[i, csd_matrix, freq, tf])
                    results.append(_re)
                pool.close()
                pool.join()
                noise_list = []
                for i in range(N):
                    Nii = results[i].get()
                    noise_list.append(Nii)

                # Plot and write results
                fig = pyplot.figure(dpi=150)
                ax = fig.gca()
                for i in range(N):
                    ax.semilogx(freq, 10*np.log10(np.abs(noise_list[i])), 'b',
                                alpha=0.1)
                    data = {'frequency': np.around(freq, 4),
                            'noise': noise_list[i]}
                    df = pd.DataFrame(data, columns=['frequency', 'noise'])
                    csvpath = os.path.join(self.outpath,
                                           "{}_selfnoise.csv".format(st[i].id))
                    df.to_csv(csvpath, index=False)


if __name__ == "__main__":
    fdsnws_url = "http://pise.u-strasbg.fr:8081"
    stations_csv = os.path.join(os.environ["PYSE_ROOT"], "etc", "stations.csv")
    start = UTCDateTime("2024-04-04T05")
    end = UTCDateTime("2024-04-04T07")
    fmin = 1
    fmax = 60
    outpath = "."
    sn_estimator = SelfnoiseEstimator(fdsnws_url, stations_csv, outpath, start,
                                      end, fmin, fmax, 100, 0.7)
    sn_estimator.get_streams()
    sn_estimator.align_streams()
    sn_estimator.process()
