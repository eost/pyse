/* Arduino CT-EW1 librairie
 * Ecrit par Maxime Bès de Berc
 * 25 septembre 2012
 *
 * Cette librairie est utilisée avec la carte de contrôle CT-EW1/Arduino développée par l'EOST
 *
*/

#include "CTEW1.h"

//Constructeur
CTEW1::CTEW1(){
	//Ne fait rien
}


//setup en mode "peak" par défaut
//un mode doit être forcément activé
void CTEW1::init(void){
	pinMode(STEP_PIN, OUTPUT);
	pinMode(MODE_PIN, OUTPUT);
	
	digitalWrite(STEP_PIN, LOW);
	digitalWrite(MODE_PIN, LOW);
}

void CTEW1::step(void){
	digitalWrite(STEP_PIN, HIGH);
	delay(200);
	digitalWrite(STEP_PIN, LOW);		
}

void CTEW1::centerMode(void){	
	digitalWrite(MODE_PIN, HIGH);
}

void CTEW1::peakMode(void){
	digitalWrite(MODE_PIN, LOW);
}



