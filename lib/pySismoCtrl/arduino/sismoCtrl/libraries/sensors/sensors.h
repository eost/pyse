/* Arduino sismoCtrl library
 *
 * This library is used for control a sismoCtrl arduino shield. Plugged in
 * front of a Centaur (or Taurus), it allows control in a single script every
 * digital outputs instead of the digitizer, inject a custom input signal, and
 * is compatible with most of standard seismometer models (t120, t240, cmg40),
 * EXCEPT STS2.
 *
 */

//Assign digital output, see shield schematic for details.
//This implies seismometer cable is built in this way:
//SEN_CAL[123] dedicated to calibration input signal (connected to BNC input of
//module)
//SEN_CTRL[456] dedicated to enable calibration
//SEN_CTRL1, SEN_CTRL2, SEN_CTRL3  dedicated to xyz/uvw mode, recentering,
//lp/sp mode, ... according to the cable scheme
//Otherwise, this module (or this cable, up to you) is not compatible
#define SEN_CTRL456_PIN	0
#define SEN_CTRL1_PIN	1
#define SEN_CTRL2_PIN	2
#define SEN_CTRL3_PIN	3

#include <Arduino.h>

#ifndef Sismo_h
#define Sismo_h

class Sismo
{
	public:
		//Constructeur
		Sismo();

		void cal(bool);
		void ctrl1(bool);
		void ctrl2(bool);
		void ctrl3(bool);
		void close(void);
};
#endif
