# -*- coding: utf-8 -*-
"""
Guralp monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""

from abc import ABC, abstractmethod
from datetime import datetime
import dateutil.parser
import logging
from lxml import etree
try:
    from urllib.error import URLError
    from urllib.parse import urlencode
    from urllib.request import urlopen
except ImportError:
    from urllib import urlencode
    from urllib2 import urlopen, URLError


logger = logging.getLogger(__name__)


class GuralpThresholds:
    """
    Store Guralp warning and critical thresholds.
    """


class Guralp:
    def __init__(self, address, port):
        self.url = 'http://%s:%s/cgi-bin/xmlstatus.cgi' % (address, port)
        self.namespaces = {
            'ns': 'http://www.guralp.com/platinum/xmlns/xmlstatus/1.1',
        }

    def _get_root(self, url):
        # Add POST data to get the XML
        raw_data = {
            'download_xml': 'Download snapshot as XML',
        }
        # url = 'http://www.w3schools.com/xml/note.xml'
        data = urlencode(raw_data).encode('utf-8')
        response = urlopen(url, data=data, timeout=15)

        return etree.parse(response)


class GuralpConfig(Guralp):
    def __init__(self, address, port):
        super().__init__(address, port)

        self.instrument_1 = 'Instrument 1'
        self.instrument_2 = 'Instrument 2'
        self.instrument_3 = 'Instrument 3'
        self.instrument_4 = 'Instrument 4'

    def import_config(self):
        try:
            root = self._get_root(self.url)
        except URLError as e:
            logger.error(e)
            return

        path = "//ns:module[contains(@path, 'instrument')]/@path"
        module = root.xpath(path, namespaces=self.namespaces)

        if len(module) < 4:
            return

        self.instrument_1 = module[0][11:]
        self.instrument_2 = module[1][11:]
        self.instrument_3 = module[2][11:]
        self.instrument_4 = module[3][11:]


class GuralpData(Guralp):
    """
    Store Guralp status data
    """
    def __init__(self, address, port):
        super().__init__(address, port)

        self.port_A = Port('A')
        self.port_B = Port('B')
        self.scream_server = ScreamServer()
        self.gdi_link_tx = GdiLinkTransmitter()
        self.gcf_compressor = GCFCompressor()
        self.miniseed_compressor = MiniSEEDCompressor()
        self.instrument_1 = Instrument(1)
        self.instrument_2 = Instrument(2)
        self.instrument_3 = Instrument(3)
        self.instrument_4 = Instrument(4)
        self.ntp = NTP()
        self.seedlink_network_server = SEEDLinkNetworkServer()
        self.storage = Storage()
        self.system = System()

    def import_data(self):
        """
        Return a Guralp object containing the monitored data.
        """
        try:
            root = self._get_root(self.url)
        except URLError as e:
            logger.error(e)
            return

        self.port_A.parse(root, self.namespaces)
        self.port_B.parse(root, self.namespaces)
        self.scream_server.parse(root, self.namespaces)
        self.gdi_link_tx.parse(root, self.namespaces)
        self.gcf_compressor.parse(root, self.namespaces)
        self.miniseed_compressor.parse(root, self.namespaces)
        self.instrument_1.parse(root, self.namespaces)
        self.instrument_2.parse(root, self.namespaces)
        self.instrument_3.parse(root, self.namespaces)
        self.instrument_4.parse(root, self.namespaces)
        self.ntp.parse(root, self.namespaces)
        self.seedlink_network_server.parse(root, self.namespaces)
        self.storage.parse(root, self.namespaces)
        self.system.parse(root, self.namespaces)


class Module(ABC):
    """
    Abstract class used to parse module block in XML file.
    """
    def __init__(self, module_path):
        self.module_path = module_path
        self.module = None
        self.namespaces = None

    def load(function):
        """
        Decorator to load module before parsing. If the module can't be
        parsed, the parse method isn't call.
        """
        def wrapper(self, root, namespaces):
            try:
                self.module = root.xpath(self.module_path,
                                         namespaces=namespaces)[0]
            except IndexError:
                pass
            else:
                function(self, root, namespaces)
        return wrapper

    @abstractmethod
    @load
    def parse(self, root, namespaces):
        """
        Parse the module and extract data.
        """
        self.namespaces = namespaces

    def _parse_int(self, xpath, factor=1, divisor=1):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            return int(element[0]) * factor // divisor
        except (IndexError, TypeError, ValueError):
            return 'U'

    def _parse_float(self, xpath, factor=1, divisor=1):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            return float(element[0]) * factor / divisor
        except (IndexError, TypeError, ValueError):
            return 'U'

    def _parse_percent(self, xpath):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            return float(element[0][:-1])
        except (IndexError, TypeError, ValueError):
            return 'U'

    def _parse_timestamp(self, xpath, factor=1, divisor=1):
        element = self.module.xpath(xpath, namespaces=self.namespaces)
        try:
            timestamp = dateutil.parser.parse(element[0])
        except IndexError:
            return 'U'
        else:
            timestamp = timestamp.replace(tzinfo=None)
            delta = datetime.utcnow() - timestamp
            return delta.total_seconds() * factor / divisor


class Port(Module):
    def __init__(self, port_value):
        module_path = \
            "//ns:module[@path='gcf-in-brp.Port%s']" % port_value
        super().__init__(module_path)

        self.blocks_received = 'U'
        self.bytes_received = 'U'
        self.naks_sent = 'U'
        self.packet_received_time = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.blocks_received = \
            self._parse_int("./ns:reading[@path='last5_blocks_in']/text()")
        self.bytes_received = self._parse_int(
            "./ns:reading[@path='bytes_in']/text()", divisor=1024)
        self.naks_sent = \
            self._parse_int("./ns:reading[@path='last5_naks']/text()")

        self.packet_received_time = self._parse_timestamp(
            "./ns:reading[@path='latest_timestamp']/text()")


class ScreamServer(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gcf-out-scream.default']")

        self.blocks_sent = 'U'
        self.clients = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.blocks_sent = \
            self._parse_int("./ns:reading[@path='last5_blocks']/text()")
        self.clients = \
            self._parse_int("./ns:reading[@path='num_clients']/text()")


class GdiLinkTransmitter(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gdi-link-tx.default']")

        self.bytes_sent = 'U'
        self.transmit_rate = 'U'
        self.clients = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.bytes_sent = self._parse_int(
            "./ns:reading[@path='last5_bytes_sent']/text()", divisor=1024)
        self.transmit_rate = \
            self._parse_float("./ns:reading[@path='tx_rate']/text()")
        self.clients = \
            self._parse_int("./ns:reading[@path='num_clients']/text()")


class GCFCompressor(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gdi2gcf.default']")

        self.samples = 'U'
        self.blocks_out = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.samples = \
            self._parse_int("./ns:reading[@path='last5_samples_in']/text()")
        self.blocks_out = \
            self._parse_int("./ns:reading[@path='last5_blocks_out']/text()")


class MiniSEEDCompressor(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='gdi2miniseed.default']")

        self.samples = 'U'
        self.text_samples = 'U'
        self.records_out = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.samples = \
            self._parse_int("./ns:reading[@path='last5_samples_in']/text()")
        self.text_samples = \
            self._parse_int("./ns:reading[@path='last5_text_in']/text()")
        self.records_out = \
            self._parse_int("./ns:reading[@path='last5_ms_rec_out']/text()")


class Instrument(Module):
    def __init__(self, instrument_nb):
        module_path = "//ns:module[contains(@path, 'instrument')][%s]" \
                      % instrument_nb
        super().__init__(module_path)

        self.clock_difference = 'U'
        self.clock_last_locked_time = 'U'
        self.mass_z = 'U'
        self.mass_n = 'U'
        self.mass_e = 'U'
        self.tap_table_lookup = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.clock_difference = self._parse_float(
            "./ns:reading[@path='clock_diff']/text()", factor=1e9)
        self.clock_last_locked_time = self._parse_timestamp(
            "./ns:reading[@path='clock_last_locked']/text()")
        self.mass_z = \
            self._parse_percent("./ns:reading[@path='mass_z']/text()")
        self.mass_n = \
            self._parse_percent("./ns:reading[@path='mass_n']/text()")
        self.mass_e = \
            self._parse_percent("./ns:reading[@path='mass_e']/text()")
        self.tap_table_lookup = \
            self._parse_int("./ns:reading[@path='tap_table_lookup']/text()")


class NTP(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='ntp']")

        self.estimated_error = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.estimated_error = self._parse_float(
            "./ns:reading[@path='estimated_error']/text()", factor=1e6)


class SEEDLinkNetworkServer(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='seedlink-out.0']")

        self.records = 'U'
        self.clients = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.records = \
            self._parse_int("./ns:reading[@path='last5_records']/text()")
        self.clients = \
            self._parse_int("./ns:reading[@path='num_clients']/text()")


class Storage(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='storage']")

        self.last_accessed_time = 'U'
        self.used_space = 'U'
        self.storage_size = 'U'
        self.used_space_percentage = 'U'
        self.power_duty_cycle = 'U'
        self.power_on_time = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.last_accessed_time = self._parse_timestamp(
            "./ns:reading[@path='last_accessed']/text()", divisor=86400)
        self.storage_size = self._parse_int(
            "./ns:reading[@path='size']/text()", divisor=1024**3)

        available_space_path = "./ns:reading[@path='free_space']/text()"
        available_space = self.module.xpath(available_space_path,
                                            namespaces=namespaces)
        try:
            available_space = int(available_space[0]) / (1024**3)
            self.used_space = self.storage_size - available_space
        except (IndexError, TypeError, ValueError):
            pass

        try:
            self.used_space_percentage = \
                (self.used_space / self.storage_size) * 100
        except TypeError:
            pass

        power_duty_cycle_path = "./ns:reading[@path='power_duty_cycle']/text()"
        self.power_duty_cycle = self._parse_percent(power_duty_cycle_path)
        self.power_on_time = \
            self._parse_int("./ns:reading[@path='power_on_time']/text()")


class System(Module):
    def __init__(self):
        super().__init__("//ns:module[@path='system']")

        self.uptime = 'U'
        self.load_average = 'U'
        self.root_free_space = 'U'
        self.root_percent_free_space = 'U'
        self.temperature = 'U'
        self.voltage = 'U'
        self.current = 'U'
        self.power = 'U'

    def parse(self, root, namespaces):
        super().parse(root, namespaces)

        self.uptime = self._parse_int(
            "./ns:reading[@path='uptime']/text()", divisor=86400)
        self.load_average = \
            self._parse_float("./ns:reading[@path='load_average']/text()")
        self.root_free_space = self._parse_int(
            "./ns:reading[@path='root_free_space']/text()", divisor=1024**2)
        self.root_percent_free_space = self._parse_percent(
            "./ns:reading[@path='root_percent_free_space']/text()")
        self.temperature = \
            self._parse_float("./ns:reading[@path='temperature']/text()")
        self.voltage = \
            self._parse_float("./ns:reading[@path='systemvoltage']/text()")
        self.current = self._parse_float(
            "./ns:reading[@path='systemcurrent']/text()", factor=1e3)
        self.power = \
            self._parse_float("./ns:reading[@path='systempower']/text()")
