# -*- coding: utf-8 -*-
"""
Computes performances of a basic seismic acquisition system (geophone + \
digitizer) in terms of electronic noise compared to Peterson's noise models \
and Clinton's averaged levels of seismicity. It also returns a shunt resistor \
value to set desired damping coefficient, and the effective sensitivity of \
whole system.

A suitable executable (geophone_spec_helper) is available in pyse/bin.

:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:licence:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""
from matplotlib import pyplot
import numpy as np
from obspy.signal.spectral_estimation import get_nlnm, get_nhnm
from scipy.signal import freqs_zpk


class geophone_spec_helper:

    def __init__(self, acq_data, seismicity_data, fmin=0.01, fmax=200):
        """
        Init geophone_spec_helper with acq_data and seismicity_data dicts.
        Needed keys and units in acq_data:
        'actual-damping': [None],
        'normalization-frequency': [Hz],
        'sensor-sensitivity': [V.s.m**-1],
        'sensor-frequency': [Hz],
        'sensor-mechanical-damping': [None],
        'sensor-coil-resistance': [Ohms],
        'sensor-coil-excursion': [m],
        'sensor-moving-mass': [kg],
        'analog-stage-impedance': [Ohms],
        'analog-stage-gain': [None],
        'analog-stage-full-scale': [Vpp],
        'datalogger-gain': [counts.V**-1],
        'datalogger-noise': [V.Hz**-2]
        """
        # Parse arguments
        self.actual_damping = float(acq_data['actual-damping'])
        self.normalization_frequency = \
            float(acq_data['normalization-frequency'])
        self.sensor_sensitivity = float(acq_data['sensor-sensitivity'])
        self.sensor_frequency = float(acq_data['sensor-frequency'])
        self.sensor_damping = float(acq_data['sensor-mechanical-damping'])
        self.sensor_coil_resistance = float(acq_data['sensor-coil-resistance'])
        self.sensor_coil_excursion = float(acq_data['sensor-coil-excursion'])
        self.sensor_moving_mass = float(acq_data['sensor-moving-mass'])
        self.analog_stage_impedance = float(acq_data['analog-stage-impedance'])
        self.analog_stage_gain = int(acq_data['analog-stage-gain'])
        self.datalogger_gain = float(acq_data['datalogger-gain'])
        self.datalogger_noise = float(acq_data['datalogger-noise'])
        self.analog_stage_full_scale = \
            float(acq_data['analog-stage-full-scale'])
        # self.datalogger_gain = int(args['--datalogger-gain'])
        # Calculate ideal total impedance: geophone + analog stage
        self.total_impedance = self.__calc_total_impedance()
        # Calculate ideal load impedance
        self.load_impedance = self.total_impedance - \
            self.sensor_coil_resistance
        # Calculate needed shunt resistance to set real impedance to ideal
        self.shunt_resistance = self.__calc_shunt_resistor()
        # Calculate sensor effective sensitivity
        self.sensor_effective_sensitivity = self.__calc_effective_sensitivity()
        # Calculate poles and zeros
        self.sensor_zeros, self.sensor_poles = self.__calc_pz()

        # Compute frequencies, pulsations and transfer function to work with
        self.frequencies = np.geomspace(fmin, fmax, num=10000)[1:]
        # Compute sensor transfer function: M/S -> V
        # (does not include digitizer nor analog stage)
        w, self.sensor_transfer_function_vel = \
            freqs_zpk(self.sensor_zeros, self.sensor_poles,
                      self.sensor_effective_sensitivity,
                      worN=2*np.pi*self.frequencies)
        self.sensor_transfer_function_acc = \
            self.sensor_transfer_function_vel / w
        # Compute geophone self-noise in V**2
        self.sensor_noise = self.__compute_geophone_self_noise()
        # Compute sampling noise
        self.datalogger_noise_acc = self.datalogger_noise**2 / \
            (self.analog_stage_gain * self.sensor_transfer_function_acc)**2
        # Compute sensor clip level
        self.sensor_clip = (self.sensor_coil_excursion*self.frequencies**2)**2
        # Compute analog stage clip level
        self.analog_stage_clip = (self.analog_stage_full_scale *
                                  self.frequencies /
                                  (self.analog_stage_gain *
                                   self.sensor_effective_sensitivity))**2
        # Parse Clinton's averaged levels of seismicity
        self.seismicity = seismicity_data

    def __calc_effective_sensitivity(self):
        """
        Geophone effective sensitivity calculation function. The load impedance
        (shunt // analog_impedance) and the coil resistance act as voltage
        divisor, reducing the effective geophone sensitivity.
        """
        gain = self.load_impedance / \
            (self.load_impedance+self.sensor_coil_resistance)
        return self.sensor_sensitivity*gain

    def __calc_pz(self):
        """
        Calculate poles and zeros from a cutoff frequency and a damping. Unlike
        corn_freq_2_paz from obspy, it returns a result if damping > 1.
        """
        w0 = 2*np.pi*self.sensor_frequency
        zeros = np.array([0, 0])
        if self.actual_damping < 1:
            p1 = -1*self.actual_damping*w0 - \
                1j*w0*np.sqrt(1-self.actual_damping**2)
            p2 = -1*self.actual_damping*w0 + \
                1j*w0*np.sqrt(1-self.actual_damping**2)
        elif self.actual_damping >= 1:
            p1 = -1*self.actual_damping*w0 - \
                w0*np.sqrt(self.actual_damping**2-1)
            p2 = -1*self.actual_damping*w0 + \
                w0*np.sqrt(self.actual_damping**2-1)
        poles = np.array([p1, p2])
        return (zeros, poles)

    def __calc_shunt_resistor(self):
        """
        Ideal shunt resistor calculation function. It calculates the value of
        the shunt resistor placed in parallel to the analog_stage_impedance to
        set the load_impedance.
        """
        return self.load_impedance * self.analog_stage_impedance / \
            (self.analog_stage_impedance - self.load_impedance)

    def __calc_total_impedance(self):
        """
        Ideal total impedance calculation function. It calculates the total
        impedance (geophone + analog stage) to set the ideal_damp as geophone
        damping coefficient. This is calculated from sensor sensitivity, sensor
        moving mass, electrical damping deduced from mechanical damping and
        sensor frequency.
        """
        electric_damp = self.actual_damping - self.sensor_damping
        return self.sensor_sensitivity**2 / \
            (4 * np.pi * self.sensor_frequency *
             self.sensor_moving_mass * electric_damp)

    def __compute_geophone_self_noise(self):
        """
        Geophone self-noise computation. See Havskov & Alguacil,
        Instrumentation in Earthquake Seismology, 2002, p. 56. The outputs is
        expressed in [V**2].
        """
        kB = 1.38064852e-23  # Boltzmann constant
        T = 293  # Temperature in Kelvin
        w0 = 2*np.pi*self.sensor_frequency
        # Mechanical part: PSD in m**2*s**-4*Hz**-1
        Nm = 4*kB*T * 2*self.actual_damping*w0/self.sensor_moving_mass
        # Electrical part in V**2*Hz**-1
        Nv = 4*kB*T * self.total_impedance
        # Electrical part in acceleration m**2*s**-4*Hz**-1
        Ne = Nv / np.absolute(self.sensor_transfer_function_acc**2)
        return Ne+Nm

    def plot_perf(self):
        """
        Plot noise and clip levels against N[HL]NM and Clinton's seismicity
        """
        fig = pyplot.figure()
        ax = fig.gca()

        # Plot Peterson's noise models
        (p,  Pl) = get_nlnm()
        (p,  Ph) = get_nhnm()
        f1 = 1./p
        ax.semilogx(f1, Pl, '-.k', f1, Ph, '-.k', label='N[LH]NM')

        # Plot Clinton's averaged level of events
        colors = ['r', 'g', 'b']
        types = ['local', 'regional', 'tele']
        for typ in types:
            for mag in sorted(self.seismicity[typ].keys()):
                f = np.array(self.seismicity[typ][mag]['freq'], dtype=float)
                a = np.array(self.seismicity[typ][mag]['acc_amp'],
                             dtype=float)
                ax.semilogx(f, 10*np.log10(a**2/f), '-.', alpha=0.2,
                            color=colors[types.index(typ)])

        # Plot self-noise
        ax.semilogx(self.frequencies,
                    10*np.log10(np.abs(self.sensor_noise +
                                       self.datalogger_noise_acc)),
                    ':k', label='self-noise')

        # Plot clip level
        ax.semilogx(self.frequencies,
                    10*np.log10(np.fmin(self.analog_stage_clip,
                                        self.sensor_clip)),
                    '--k', label='clip-level')

        ax.grid()
        ax.legend()
        ax.set_ylabel("Power Spectral Density [dB][m**2.s**-4.Hz**-1]")
        ax.set_xlabel("Frequency [Hz]")
        ax.set_xlim(self.frequencies[0], self.frequencies[-1])
        return fig

    def plot_tf(self):
        """
        Plot whole acquisition chain transfer function
        """
        fig = pyplot.figure()
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)

        tf = self.sensor_transfer_function_vel * self.analog_stage_gain * \
            self.datalogger_gain
        norm_indx = np.abs(self.frequencies -
                           self.normalization_frequency).argmin()
        ax1.semilogx(self.frequencies, 20*np.log10(np.abs(tf)))
        ax1.set_ylabel("Amplitude [dB][m.s**-1]")
        ax2.semilogx(self.frequencies, np.angle(tf, deg=True))
        ax2.set_ylabel("Phase [deg]")
        ax2.set_xlabel("Frequency [Hz]")
        ax1.axhline(20*np.log10(np.abs(tf[norm_indx])),
                    color='k', linestyle='-.')
        [ax.axvline(self.sensor_frequency, color='k', linestyle='--',
                    label='cut-off freq.') for ax in fig.axes]
        [ax.axvline(self.normalization_frequency, color='k', linestyle=':',
                    label='norm. freq.') for ax in fig.axes]
        [ax.grid() for ax in fig.axes]
        [ax.set_xlim(self.frequencies[0], self.frequencies[-1])
            for ax in fig.axes]
        ax1.legend()
        return fig
