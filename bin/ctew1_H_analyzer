#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    ctew1_H_analyser fdsn <start> <end> <stream> [--url=<url>] \
[--disp=<disp>] [--coeff=<coeff>] [--period=<period>] [--damping=<damping>] \
[--n-freq=<nfreq>] [--lsb=<lsb>] [--g=<g>] [--bridge-l=<bridge-l>]
    ctew1_H_analyser mseed <file> [--disp=<disp>] [--coeff=<coeff>] \
[--period=<period>] [--damping=<damping>] [--lsb=<lsb>] [--n-freq=<nfreq>] \
[--g=<g>] [--bridge-l=<bridge-l>]

Example:
    ctew1_H_analyser fdsn 2016-07-26T18:13:44 2016-07-26T18:20:07 \
XX.GPIL.00.BHE --lsb=2.388048e-6 --coeff=1.00444 --period=121.134 \
--damping=0.706287
    ctew1_H_analyser mseed data.mseed --disp=0.175e-3 --coeff=1.044

Options:
    -h --help           Show this screen.
    --version           Show version.
    <start>             Start time in obspy.core.UTCDateTime format.
    <end>               End time in obspy.core.UTCDateTime format.
    <stream>            Stream to analyse in seed code separated by dots. \
(Ex: XX.GPIL.00.BHE).
    --url=<url>         Set base url of fdsnws server, fdsn mode only. \
[default: http://dataselect.u-strasbg.fr:8080].
     --disp=<disp>       Set vertical displacement of table in meters \
                        [default: 1.75e-4].
    --coeff=<coeff>     Set line loss coeff, due to instruments impedances \
                        [default: 1.0].
    --period=<period>   Set sensor period for deconvolution [default: 120].
    --damping=<damping>  Set sensor damping for deconvolution [default: 0.707].
    --n-freq=<nfreq>    Specify normalization frequency [default: 1].
    --lsb=<lsb>         Set digitizer lsb for deconvolution \
[default: 2.5e-6].
    --g=<g>             Set gravitational constant [default: 9.80665].
    --bridge-l=<bridge-l>  Set bridge length in meters [default: 0.396].
"""

from docopt import docopt
from obspy.clients.fdsn import Client as fClient
from obspy.core import read, UTCDateTime

from pyCTEW1 import HCalc
from pyse_util import make_vel_paz

if __name__ == '__main__':
    args = docopt(__doc__, version='ctew1_H_analyzer 1.1')
    # Uncomment for debug
    # print(args)

    if args['fdsn']:
        data = fClient(base_url=args['--url'],
                       service_mappings={'event': None})
        t1 = UTCDateTime(args['<start>'])
        t2 = UTCDateTime(args['<end>'])
        code = args['<stream>'].split('.')
        st = data.get_waveforms(code[0], code[1], code[2], code[3], t1, t2)

    elif args['mseed']:
        st = read(args['<file>'])

    print(st)
    tr = st[0]
    _paz = make_vel_paz(float(args['--period']),
                        float(args['--damping']),
                        1./float(args['--lsb']),
                        float(args['--n-freq']))

    (G, D, S) = HCalc(tr, _paz, float(args['--disp']), float(args['--coeff']),
                      float(args['--bridge-l']), float(args['--g']))
    print("Mean values for %s:" % (tr.id))
    print("    Gain: %.3f V*s*m**-1." % (G))
    print("    Standard deviation: %.3f" % (D))
    print("    SNR: %.3f" % (S))
