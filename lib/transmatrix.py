# -*- coding: utf-8 -*-
"""
    Function calculating a rotation matrix between a stream of 3 orthogonal \
    traces (ouput of unknown seismometer) and another stream of 3 orthogonal \
    reference traces (output of known seismometer) recording the same signal. \
    For each trace of unknown stream, it uses the linear method to express it \
    as a combination of the reference stream. Therefore, the result is 3 \
    coeff per unknown channel, ie a 3x3 matrix:
        |X|   |a00 a10 a20|   |Xref|
        |Y| = |a01 a11 a21| x |Yref|
        |Z|   |a02 a12 a22|   |Zref|
    Finally, that supposes strong coherence between signals. It is therefore \
    necessary to filter the signals over an appropriate range (ie. \
    micro-seismic peak).

:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
import numpy as np

from pyse_util import resample_traces, verify_stream


def transmatrix(res_stream, mon_stream, fmin=0.1, fmax=10):
    """
    Rotation matrix calculation function.
    :type res_stream: Stream object from module obspy.core.stream
    :param res_stream: Stream containing 3 traces with the signal from the \
    unknown seismometer.
    :type mon_stream: Stream object from module obspy.core.stream
    :param mon_stream: Stream containing 3 traces with the signal from the \
    known seismometer.
    :type fmin: float
    :param fmin: Minimal frequency of the bandpass filter applied.
    :type fmax: float
    :param fmax: Maximal frequency of the bandpass filter applied.
    """
    m_stream = mon_stream.copy()
    r_stream = res_stream.copy()

    for m in (m_stream, r_stream):

        if len(m) != 3:
            print("[lib.transmatrix]: Stream must strictly have 3 traces")
            raise SystemExit

        verify_stream(m)

        # Detrend, taper and filter stream
        m.detrend('demean')
        m.detrend('linear')
        m.taper(0.2)
        m.filter('bandpass', freqmin=fmin, freqmax=fmax, zerophase=True,
                 corners=8)

    # Verify streams between them: same sampling rate?
    for i in range(3):
        resample_traces(m_stream[i], r_stream[i])

    verify_stream((m_stream+r_stream))

    # Create matrix of data with shape 3 x npts
    coeff_matrix = np.array([m_stream[0].data, m_stream[1].data,
                             m_stream[2].data]).transpose()

    # Create empty matrix
    matrix = np.array([])
    # Feed it with 9 coefficients calculated with linear regression
    for i in range(3):
        matrix = np.append(matrix, np.linalg.lstsq(coeff_matrix,
                                                   r_stream[i].data,
                                                   rcond=-1)[0])
    # Reshape correctly the final matrix
    matrix = np.reshape(matrix, (3, 3))

    return matrix


def tait_bryan_zyx_2_matrix(alpha, beta, gamma):
    """
    Ideal rotation matrix calculation function, from given tait-bryan angles.
    Returns an ideal rotation matrix.
    :type alpha: Float
    :param alpha: Rotation angle using Z axis.
    :type beta: Float
    :param beta: Rotation angle using Y axis.
    :type gamma: Float
    :param gamma: Rotation angle using Y axis.
    :returns: Ideal rotation matrix (np.ndarray)
    """
    sa = np.sin(alpha)
    ca = np.cos(alpha)
    sb = np.sin(beta)
    cb = np.cos(beta)
    sc = np.sin(gamma)
    cc = np.cos(gamma)
    matrix = np.array([[ca*cb, -sa*cc+ca*sb*sc, -1*(sa*sb+ca*sb*cc)],
                       [sa*cb, ca*cc+sa*sb*sc, ca*sc-1*sa*sb*cc],
                       [sb, -1*cb*sc, cb*cc]])
    return matrix


def matrix_2_tait_bryan_zyx(matrix):
    """
    Calculates Tait-Bryan angles from rotation matrix.
    :type matrix: numpy.ndarray
    :param matrix: Rotation matrix calculated by transmatrix.
    :returns: Angles vector (numpy.ndarray([alpha, beta, gamma])),
    standard deviations on angles
    (numpy.ndarray([alpha_std, beta_std, gamma_std])),
    gains vector (numpy.ndarray([Gx, Gy, Gz]))
    """

    # Calculates angles using division of common colomn terms
    alpha = np.arctan2(matrix[1, 0], matrix[0, 0])
    beta = np.arctan2(np.cos(alpha)*matrix[2, 0], matrix[0, 0])
    tg_num = np.cos(beta)*matrix[1, 2]/matrix[2, 2] + \
        np.sin(alpha)*np.sin(beta)
    gamma = np.arctan2(tg_num, np.cos(alpha))

    # Compute ideal rotation matrix from angles
    R = tait_bryan_zyx_2_matrix(alpha, beta, gamma)
    # Extract from inverse matrix the gains vector
    GE = np.dot(matrix, np.linalg.inv(R))
    G = GE.diagonal()

    # Compute ideal elemental rotation matrixes
    Ra = tait_bryan_zyx_2_matrix(alpha, 0, 0)
    Rb = tait_bryan_zyx_2_matrix(0, beta, 0)
    Rc = tait_bryan_zyx_2_matrix(0, 0, gamma)

    # Calculates elemental rotation matrices for each angle, and retrieve the
    # deviation by row
    res_alpha = np.divide(matrix, G)
    res_alpha = np.dot(res_alpha, np.linalg.inv(Rc))
    res_alpha = np.dot(res_alpha, np.linalg.inv(Rb))
    alpha1 = np.arctan2(-1*res_alpha[0, 1], res_alpha[0, 0])
    alpha2 = np.arctan2(res_alpha[1, 0], res_alpha[1, 1])
    alpha_std = np.abs(alpha2-alpha1)

    res_beta = np.divide(matrix, G)
    res_beta = np.dot(res_beta, np.linalg.inv(Rc))
    res_beta = np.dot(res_beta, np.linalg.inv(Ra))
    beta1 = np.arctan2(-1*res_beta[0, 2], res_beta[0, 0])
    beta2 = np.arctan2(res_beta[2, 0], res_beta[2, 2])
    beta_std = np.abs(beta2-beta1)

    res_gamma = np.divide(matrix, G)
    res_gamma = np.dot(res_gamma, np.linalg.inv(Rb))
    res_gamma = np.dot(res_gamma, np.linalg.inv(Ra))
    gamma1 = np.arctan2(-1*res_gamma[2, 1], res_gamma[2, 2])
    gamma2 = np.arctan2(res_gamma[1, 2], res_gamma[1, 1])
    gamma_std = np.abs(gamma2-gamma1)

    return (np.array([alpha, beta, gamma]),
            np.array([alpha_std, beta_std, gamma_std]),
            G)
