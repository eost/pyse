#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

 Usage:
    pyFdsnFetch <start> <end> <stream> [-o <output_file>] [--url=<url>] \
[--stationxml] [--remove-response]

Options:
    -h --help           Show this screen.
    --version           Show version.
    <start>             Start time in obspy.core.UTCDateTime format.
    <end>               End time in obspy.core.UTCDateTime format.
    <stream>            Stream to analyse in seed code separated by dots. \
Multiple streams are allowed, separated by comma \
(Ex: XX.GPIL.00.HH?,FR.STR.00.HH?).
    -o <output_file>    Set output file.
    --url=<url>         Set base url of fdsnws server, fdsn mode only. \
[default: http://dataselect.u-strasbg.fr:8080]
    --stationxml        Get stationxml and write it instead of time series.
    --remove-response   Remove response from time series.
"""

from docopt import docopt
from obspy.clients.fdsn import Client
from obspy.core import UTCDateTime, Stream


if __name__ == '__main__':
    args = docopt(__doc__, version='pyArclinkFetch 1.0')
    # Uncomment for debug
    # print(args)

    data = Client(base_url=args['--url'], service_mappings={'event': None})
    t1 = UTCDateTime(args['<start>'])
    t2 = UTCDateTime(args['<end>'])
    streams = args['<stream>'].split(',')
    st = Stream()
    for stream in streams:
        code = stream.split('.')
        if args['--stationxml'] is False:
            st += data.get_waveforms(code[0], code[1], code[2], code[3], t1,
                                     t2, attach_response=True)
        else:
            inv_ref = data.get_stations(network=code[0], station=code[1],
                                        location=code[2], channel=code[3],
                                        starttime=t1, endtime=t2,
                                        level='response')
            inv_ref.write("%s.xml" % stream, format="STATIONXML")

    if len(st):
        print(st)
        if args['--remove-response']:
            for tr in st:
                pre_filt = (0.001, 0.002, tr.stats.sampling_rate*0.4,
                            tr.stats.sampling_rate*0.5)
                tr.remove_response(pre_filt=pre_filt)
        st.plot(equal_scale=False)
        if args['-o'] is not None:
            st.write(args['-o'], format='MSEED')
