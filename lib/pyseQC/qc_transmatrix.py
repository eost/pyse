#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np
from obspy.core import UTCDateTime
import yaml

from transmatrix import transmatrix, matrix_2_tait_bryan_zyx
from pyse_util import sort_stream


def _qc_transmatrix(transmatrix_requests, server, conf):

    if len(transmatrix_requests) > 0:
        gains = np.array([])
        angles = np.array([])
        devs = np.array([])
        paz = None

        if 'paz_file' in conf['main'].keys():
            paz_yaml = open(conf['main']['paz_file'])
            paz = yaml.load(paz_yaml)
            paz_yaml.close()
            for prop_name, prop_val in paz.items():
                if 'unit' not in prop_name and isinstance(prop_val, str):
                    paz[prop_name] = float(prop_val)
                elif isinstance(prop_val, list):
                    new_list = list()
                    for elem in prop_val:
                        new_list.append(complex(elem))
                    paz[prop_name] = new_list
            nfreq = paz['normalization_frequency']
            paz['gain'] = np.abs(np.prod(2j*np.pi*nfreq -
                                         np.array(paz['poles'])) /
                                 np.prod(2j*np.pi*nfreq -
                                         np.array(paz['zeros'])))

        i = 0
        for request in transmatrix_requests:
            print("%d %s" % (i, request))
            i += 1
            words = request.split(' ')
            t1 = UTCDateTime(words[2])
            t2 = UTCDateTime(words[3])
            stream = words[4].replace(words[4][-3:-1],
                                      conf['qc_transmatrix']['stream'])
            seed_codes = stream.split('.')

            try:
                st = server.get_waveforms(seed_codes[0], seed_codes[1],
                                          seed_codes[2], seed_codes[3], t1, t2,
                                          attach_response=True)
                ref_stream = conf['qc_transmatrix']['ref_stream']

                for word in words:
                    if word[:12] == '--ref-stream':
                        ref_stream = word.split('=')[1]
                    if word[:13] == '--reverse-pol':
                        comp2reverse = word.split('=')[1]
                        for comp in comp2reverse:
                            st0 = st.select(component=comp.upper())
                            for tr in st0:
                                tr.data *= -1

                code = ref_stream.split('.')
                st_ref = server.get_waveforms(code[0], code[1], code[2],
                                              code[3], t1, t2,
                                              attach_response=True)
                for tr in st_ref:
                    try:
                        prefilt = (0.01, 0.02,
                                   tr.stats.sampling_rate*0.4,
                                   tr.stats.sampling_rate*0.5)
                        tr.remove_response(pre_filt=prefilt)
                    except AttributeError:
                        pass

                st.trim(t1, t2)
                st_ref.trim(t1, t2)
                st = sort_stream(st)
                st_ref = sort_stream(st_ref)

                for tr in st:
                    prefilt = (0.01, 0.02, tr.stats.sampling_rate*0.4,
                               tr.stats.sampling_rate*0.5)
                    if paz is not None:
                        tr.simulate(paz_remove=paz, water_level=60,
                                    pre_filt=prefilt)
                    else:
                        try:
                            tr.remove_response(water_level=60,
                                               pre_filt=prefilt)
                        except AttributeError:
                            pass

                f_min = conf['qc_transmatrix']['fmin']
                f_max = conf['qc_transmatrix']['fmax']
                t_mat = transmatrix(st, st_ref, fmin=f_min, fmax=f_max)
                R, d, G = matrix_2_tait_bryan_zyx(t_mat)
                angles = np.append(angles, R)
                gains = np.append(gains, G)
                devs = np.append(devs, d)

            except Exception as e:
                print(e)

        angles = angles.reshape(int(angles.size/3), 3)
        gains = gains.reshape(int(gains.size/3), 3)
        devs = devs.reshape(int(devs.size/3), 3)

        if gains.size > 0 and gains.size % 3 == 0:
            print("Transmatrix statistics:")
            print()
            print("Median yaw: %.3f degrees" %
                  (np.median(angles[:, 0]*180/np.pi)))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f deg"
                  % (angles[:, 0].min()*180/np.pi,
                     np.percentile(angles[:, 0]*180/np.pi, 25),
                     np.percentile(angles[:, 0]*180/np.pi, 75),
                     angles[:, 0].max()*180/np.pi))
            print("Median yaw std: %.3f degrees" %
                  (np.median(devs[:, 0]*180/np.pi)))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f deg"
                  % (devs[:, 0].min()*180/np.pi,
                     np.percentile(devs[:, 0]*180/np.pi, 25),
                     np.percentile(devs[:, 0]*180/np.pi, 75),
                     devs[:, 0].max()*180/np.pi))
            print()

            print("Median pitch: %.3f degrees" %
                  (np.median(angles[:, 1]*180/np.pi)))
            print("  Interquartile (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f deg"
                  % (angles[:, 1].min()*180/np.pi,
                     np.percentile(angles[:, 1]*180/np.pi, 25),
                     np.percentile(angles[:, 1]*180/np.pi, 75),
                     angles[:, 1].max()*180/np.pi))
            print("Median pitch std: %.3f degrees" %
                  (np.median(devs[:, 1]*180/np.pi)))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f deg"
                  % (devs[:, 1].min()*180/np.pi,
                     np.percentile(devs[:, 1]*180/np.pi, 25),
                     np.percentile(devs[:, 1]*180/np.pi, 75),
                     devs[:, 1].max()*180/np.pi))
            print()

            print("Median roll: %.3f degrees" %
                  (np.median(angles[:, 2]*180/np.pi)))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f deg"
                  % (angles[:, 2].min()*180/np.pi,
                     np.percentile(angles[:, 2]*180/np.pi, 25),
                     np.percentile(angles[:, 2]*180/np.pi, 75),
                     angles[:, 2].max()*180/np.pi))
            print("Median roll std: %.3f degrees" %
                  (np.median(devs[:, 2]*180/np.pi)))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f deg"
                  % (devs[:, 2].min()*180/np.pi,
                     np.percentile(devs[:, 2]*180/np.pi, 25),
                     np.percentile(devs[:, 2]*180/np.pi, 75),
                     devs[:, 2].max()*180/np.pi))
            print()

            print("Median X relative gain: %.3f" % np.median(gains[:, 0]))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f"
                  % (gains[:, 0].min(),
                     np.percentile(gains[:, 0], 25),
                     np.percentile(gains[:, 0], 75),
                     gains[:, 0].max()))
            print("Median Y relative gain: %.3f" % np.median(gains[:, 1]))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f"
                  % (gains[:, 1].min(),
                     np.percentile(gains[:, 1], 25),
                     np.percentile(gains[:, 1], 75),
                     gains[:, 1].max()))
            print("Median Z relative gain: %.3f" % np.median(gains[:, 2]))
            print("  Interquartiles (0, 25, 75, 100): %.3f/%.3f/%.3f/%.3f"
                  % (gains[:, 2].min(),
                     np.percentile(gains[:, 2], 25),
                     np.percentile(gains[:, 2], 75),
                     gains[:, 2].max()))
            print()

            ft = pyplot.figure('transmatrix', dpi=150)
            axypr = ft.add_subplot(311)
            axypr.plot(angles[:, 0]*180/np.pi, '.r', label='Yaw')
            axypr.plot(angles[:, 1]*180/np.pi, '.g', label='Pitch')
            axypr.plot(angles[:, 2]*180/np.pi, '.b', label='Roll')
            axypr.grid()
            axypr.legend(loc='best')
            axypr.set_ylabel('Angles [deg]')
            axd = ft.add_subplot(312)
            axd.plot(devs[:, 0]*180/np.pi, '.r', label='Yaw')
            axd.plot(devs[:, 1]*180/np.pi, '.g', label='Pitch')
            axd.plot(devs[:, 2]*180/np.pi, '.b', label='Roll')
            axd.grid()
            axd.set_ylabel('Orthogonality errors [deg]')
            axd.legend(loc='best')
            axg = ft.add_subplot(313)
            axg.plot(gains[:, 0], '.b', label='X rel. gain')
            axg.plot(gains[:, 1], '.g', label='Y rel. gain')
            axg.plot(gains[:, 2], '.r', label='Z rel. gain')
            axg.grid()
            axg.legend(loc='best')
            axg.set_ylabel('Relative gains')
            ft.suptitle("Transmatrix statistics")

            return ft
