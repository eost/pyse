# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import mlab
import numpy as np
from obspy.core import Stream
from obspy.core.inventory.response import Response
from obspy.signal.invsim import corn_freq_2_paz, cosine_taper
from obspy.signal.util import prev_pow_2
from scipy.signal import zpk2tf, freqs
import yaml


def basic_detrend_func(data):
    data = mlab.detrend_mean(data)
    data = mlab.detrend_linear(data)
    return data


def calc_norm_factor(paz, nfreq):
    paz['gain'] = np.abs(np.prod(2j*np.pi*nfreq - np.array(paz['poles'])) /
                         np.prod(2j*np.pi*nfreq - np.array(paz['zeros'])))
    return paz


def calc_custom_coh(tr1, tr2, **kwargs):
    if 'n_fft' in kwargs:
        n_fft = kwargs['n_fft']
    else:
        n_fft = prev_pow_2(int(tr1.stats.npts*1./13))

    if 'n_overlap' in kwargs:
        n_overlap = kwargs['n_overlap']
    else:
        n_overlap = int(n_fft*0.75)

    (C, f) = mlab.cohere(tr1.data, tr2.data,
                         Fs=tr1.stats.sampling_rate, NFFT=n_fft,
                         noverlap=n_overlap, sides='onesided',
                         detrend=basic_detrend_func,
                         window=cosine_taper(n_fft, p=0.2),
                         scale_by_freq=True)
    C = C[1:]
    f = f[1:]
    return C, f


def calc_custom_psd(tr, **kwargs):
    if 'n_fft' in kwargs:
        n_fft = kwargs['n_fft']
    else:
        n_fft = prev_pow_2(int(tr.stats.npts*1./13))  # default as McNamara

    if 'n_overlap' in kwargs:
        n_overlap = kwargs['n_overlap']
    else:
        n_overlap = int(n_fft*0.75)  # default as McNamara

    (P, f) = mlab.psd(tr.data, Fs=tr.stats.sampling_rate, NFFT=n_fft,
                      noverlap=n_overlap, sides='onesided',
                      detrend=basic_detrend_func,
                      window=cosine_taper(n_fft, p=0.2),
                      scale_by_freq=True)
    P = P[1:]
    f = f[1:]
    return P, f


def deconv_vel_psd(f, P, paz):
    if isinstance(paz, dict):
        (b, a) = zpk2tf(paz['zeros'], paz['poles'],
                        paz['gain']*paz['sensitivity'])
        (w, H0) = freqs(b, a, 2*np.pi*f)
    elif isinstance(paz, Response):
        H0 = paz.get_evalresp_response_for_frequencies(f, output='VEL')
    P = P/np.absolute(H0*np.conjugate(H0))
    return P


def frac_octave_smooth(n, freq, data):
    if n > 0:
        for f in freq:
            nth_root = 2**(1/n)
            lowerf = f/nth_root
            upperf = f*nth_root
            ind = [np.abs(freq-bound).argmin()
                   for bound in sorted([lowerf, f, upperf])]
            if ind[0] == ind[2]:
                continue
            else:
                data[ind[1]] = data[ind[0]:ind[2]].mean()
    return data


def extract_paz_from_yamlcatalog(yaml_file, model):
    cat_yaml = open(yaml_file)
    cat = yaml.load(cat_yaml, Loader=yaml.SafeLoader)
    cat_yaml.close()
    return pazstr2paznumber(cat[model])


def gcd(x, y):
    while(y):
        x, y = y, x % y
    return x


def make_vel_paz(period, damping, sensitivity, n_freq):
    paz = corn_freq_2_paz(1./period, damp=damping)
    paz['sensitivity'] = sensitivity
    paz['normalization_frequency'] = n_freq
    paz['sensitivity_unit'] = 'M/S'
    paz = calc_norm_factor(paz, n_freq)
    return paz


def parse_paz_file(yaml_file):
    paz_yaml = open(yaml_file)
    paz = yaml.load(paz_yaml, Loader=yaml.SafeLoader)
    paz_yaml.close()
    return pazstr2paznumber(paz)


def pazstr2paznumber(pazstr):
    paznumber = pazstr.copy()
    for prop_name, prop_val in paznumber.items():
        if isinstance(prop_val, str) and "unit" not in prop_name:
            paznumber[prop_name] = float(prop_val)
        elif isinstance(prop_val, list):
            new_list = list()
            for elem in prop_val:
                new_list.append(complex(elem))
            paznumber[prop_name] = new_list
    paznumber = calc_norm_factor(paznumber,
                                 paznumber['normalization_frequency'])
    return paznumber


def resample_traces(tr1, tr2):
    if tr1.stats.sampling_rate != tr2.stats.sampling_rate:
        new_sampling_rate = gcd(tr1.stats.sampling_rate,
                                tr2.stats.sampling_rate)
        for t in [tr1, tr2]:
            if t.stats.sampling_rate != new_sampling_rate:
                decimation_factor = t.stats.sampling_rate / new_sampling_rate
                t.decimate(int(decimation_factor))


def rms(a_trace):
    """
    Function returning the rms array of a Trace object. Used for threshold \
    calculation.

    :param a_trace: Trace object from module obspy.core.trace.
    :returns: Array of instantaneous rms of a_trace.
    """
    return np.sqrt(np.mean(a_trace.data**2))


def sort_stream(st0, axis='enz'):
    """
    Function sorting streams before a rotation.

    :param st: stream to sort
    :param axis: defines what is the axis reference:
        xyz = nez -> useful for rotation and angles calculation against a
            reference sensor in an axis system following seed convention
            (typically transmatrix module)
        xyz = enz -> useful for rotation to uvw outputs as defined in
            broad-band systems (typically repimp module) [default].
    """
    st = st0.copy()
    st.sort()
    if axis == 'enz':
        if st[0].stats.channel[-1] == '1' and st[1].stats.channel[-1] == '2':
            return Stream(traces=[st[1], st[0], st[2]])
        else:
            return st
    elif axis == 'nez':
        if st[0].stats.channel[-1] == 'E' and st[1].stats.channel[-1] == 'N':
            return Stream(traces=[st[1], st[0], st[2]])
        else:
            return st


def verify_stream(st):
    if len(st) >= 2:
        sampling_rate = st[0].stats.sampling_rate
        npts = st[0].stats.npts
        starttime = st[0].stats.starttime

        for tr in st:
            # Verify traces: same sampling rate?
            if tr.stats.sampling_rate != sampling_rate:
                print("[util.verify_stream]: Bad sampling rate for %s" %
                      tr.id)
                raise SystemExit

            # Verify each trace of stream: same length?
            if tr.stats.npts != npts:
                print("[util.verify_stream]: Bad number of samples for %s" %
                      tr.id)
                raise SystemExit

            # Verify each trace of stream: same start time?
            if tr.stats.starttime-starttime >= tr.stats.sampling_rate/2:
                print("[util.verify_stream]: Bad start time for %s" %
                      tr.id)
                raise SystemExit
