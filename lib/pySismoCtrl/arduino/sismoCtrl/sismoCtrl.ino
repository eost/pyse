/* Arduino sismoCtrl sketch file
 * 
 * This sketch is used for control a sismoCtrl arduino shield. Plugged in front 
 * of a Q330, it allows control in a single script every digital outputs instead 
 * of the digitizer, inject a custom input signal, and is compatible with most of 
 * standard seismometer models (t120, cmg40).
 *
 */
#include <Ethernet2.h>
#include <EthernetUdp2.h>
#include <sensors.h>

#define DEBUG 0

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {0x90, 0xA2, 0xDA, 0x10, 0x3C, 0x34};

IPAddress ip(192, 168, 1, 10);

unsigned int localPort = 58888;      // Local port to listen on

// Buffer for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //buffer to hold incoming packet,
char replyBuffer[] = "Done";       // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

// Open Sismo instance, see sensors.h for details.
Sismo sismo;

void send_reply() {
  // Send a reply, to the IP address and port that sent us the packet we received
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  Udp.write(replyBuffer);
  Udp.endPacket();
  if (DEBUG) {
    Serial.println(replyBuffer);
  }
}

void setup() {
  boolean flag = false;
  // Start the Ethernet and UDP:
  Ethernet.begin(mac, ip);
  Udp.stop();
  flag = Udp.begin(localPort);
  if (DEBUG) {
    Serial.begin(9600);
    while (!Serial) ;
    Serial.println("Socket open");
    Serial.print("Ethernet address: ");
    Serial.println(Ethernet.localIP());
    Serial.print("UDP begin: ");
    Serial.println(flag);
  }
}

void loop() {
  String cmd;
  int packetSize = Udp.parsePacket();

  if(packetSize) {
    if (DEBUG){
      Serial.print("Received packet (");
      Serial.print(packetSize);
      Serial.print(" bytes) from ");
      Serial.println(Udp.remoteIP());
    }

    // Read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    cmd = packetBuffer;
    
    if (DEBUG){
       Serial.print("Command received: ");
       Serial.println(packetBuffer);
    }

   if (cmd.substring(0, 10) == "ctrl1_high"){
	    // Makes SEN_CTRL1 pin active.
      sismo.ctrl1(HIGH);
	    send_reply();
    }

    if (cmd.substring(0, 9) == "ctrl1_low"){
      // Makes SEN_CTRL1 pin inactive.
      sismo.ctrl1(LOW);
      send_reply();
    }
    
    if (cmd.substring(0, 10) == "ctrl2_high"){
     // Makes SEN_CTRL2 pin active.
     sismo.ctrl2(HIGH);
     send_reply();
    }

    if (cmd.substring(0, 9) == "ctrl2_low"){
     // Makes SEN_CTRL2 pin inactive.
     sismo.ctrl2(LOW);
     send_reply();
    }
     
    if (cmd.substring(0, 10) == "ctrl3_high"){
	    // Makes SEN_CTRL3 pin active.
      sismo.ctrl3(HIGH);
      send_reply();
    }

    if (cmd.substring(0, 9) == "ctrl3_low"){
      // Makes SEN_CTRL3 pin inactive.
      sismo.ctrl3(LOW);
      send_reply();
    }
    
    if (cmd.substring(0, 8) == "cal_high"){
	    // Makes SEN_CTRL456 pin active.
      sismo.cal(HIGH);
      send_reply();
    }

    if (cmd.substring(0, 7) == "cal_low"){
      // Makes SEN_CTRL456 pin inactive.
      sismo.cal(LOW);
      send_reply();
    }
  }
}
