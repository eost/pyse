# -*- coding: utf-8 -*-
"""
    Class for handling CTEW1 (based on arduino) module, controlling shake \
    table. See arduino/CTEW1cmd/CTEW1cmd.ino

:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
import socket


class CTEW1:
    def __init__(self, IP, port):
        """
        CTEW1 constructor. Open network socket.
        :type IP: string
        :param IP: IP address of CTEW1cmd module. Must be compatible with the\
        one specified in arduino/CTEW1cmd/CTEW1cmd.ino
        :type port: int
        :param port: UDP port used by CTEW1cmd module. Must be compatible \
        with the one specified in arduino/CTEW1cmd/CTEW1cmd.ino
        """
        self.addr = (IP, port)
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.settimeout(3)
        self.s.bind(('', port))

    def center(self):
        """
        Commands shake table in center mode.
        """
        try:
            self.s.sendto("Center\0".encode(), self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[lib.pyCTEW1.CTEW1]: Bad response from module")
        except socket.timeout:
            self.s.close()
            print("[lib.pyCTEW1.CTEW1]: No connection")

    def peak(self):
        """
        Commands shake table in peak/peak mode.
        """
        try:
            self.s.sendto("Peak\0".encode(), self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[lib.pyCTEW1.CTEW1]: Bad response from module")
        except socket.timeout:
            self.s.close()
            print("[lib.pyCTEW1.CTEW1]: Error, no connection")

    def step(self):
        """
        Commands shake table perform a step.
        """
        try:
            self.s.sendto("Step\0".encode(), self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[lib.pyCTEW1.CTEW1]: Bad response from module")
        except socket.timeout:
            self.s.close()
            print("[lib.pyCTEW1.CTEW1]: Error, no connection")

    def close(self):
        """
        Close network socket
        """
        self.s.close()
