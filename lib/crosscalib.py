from copy import deepcopy
import numpy as np
from obspy.core.inventory.response import Response
from obspy.core.util.attribdict import AttribDict
from scipy.signal import freqs_zpk

from pyse_util import frac_octave_smooth


def segment_trace(trace, length_segment, overlap):
    """
    The trace is segmented in N pieces.
    Each piece has a length equal to length_segment and overlap with
    the previous piece.
    Return a list containing N segments of the trace.
    """
    nbr_sample_segment = int(trace.stats.sampling_rate*length_segment)
    nbr_sample_overlap = int(overlap * nbr_sample_segment)
    if nbr_sample_segment > trace.stats.npts:
        msg = "parameter segment_length is too high"
        raise ValueError(msg)
    else:
        ind_deb, ind_end = (0, nbr_sample_segment)
        trace_segmented = []
        while ind_deb <= trace.stats.npts - nbr_sample_segment:
            segment = deepcopy(trace)
            segment_data = trace.data[ind_deb:ind_end]
            segment.data = segment_data
            trace_segmented.append(segment)
            ind_deb = ind_end - nbr_sample_overlap
            ind_end = ind_deb + nbr_sample_segment
        return trace_segmented


def cmp_common_frequency_vector(segment_ref, segment_test):
    """
    Compute the common frequency vector betwenn the two traces.
    The maximum frequency is linked to the lowest sampling rate of
    the two traces. The frequence difference between neighbor frequencies
    is equal to the inverse of the duration of the two traces.
    """
    nyquist_frequency_ref = segment_ref.stats.sampling_rate / 2
    nyquist_frequency_test = segment_test.stats.sampling_rate / 2
    if nyquist_frequency_ref <= nyquist_frequency_test:
        freq = np.fft.rfftfreq(segment_ref.stats.npts, segment_ref.stats.delta)
    else:
        freq = np.fft.rfftfreq(segment_test.stats.npts,
                               segment_test.stats.delta)
    return freq[1:]


def compute_spectrum(trace_segmented, freq, perc_taper):
    """
    Compute the smoothed spectrum of each segment of the trace segmented.
    Return a list, each element of the list is the fft of a trace segment.
    """
    fft_trace_segmented = []
    for seg in trace_segmented:
        seg.taper(perc_taper, beta=4*np.pi, type='kaiser')
        fft_seg = np.fft.rfft(seg.data) * seg.stats.delta
        fft_trace_segmented.append(fft_seg[1:len(freq) + 1])
    return fft_trace_segmented


def compute_tf(tr_ref, tr_test, segment_length, perc_overlap, perc_taper):
    """
    Compute the transfer function of the tested system
    using the method described by Pavlis
    (Pavlis, 1994, calibration of seismometers using ground noise).
    """
    print("Compute %s transfer function" % tr_test.id)
    [tr.detrend('demean') for tr in [tr_ref, tr_test]]
    ref_segmented = segment_trace(tr_ref, segment_length, perc_overlap)
    test_segmented = segment_trace(tr_test, segment_length, perc_overlap)
    freq = cmp_common_frequency_vector(ref_segmented[0], test_segmented[0])
    fft_ref_seg = compute_spectrum(ref_segmented, freq, perc_taper)
    fft_test_seg = compute_spectrum(test_segmented, freq, perc_taper)
    tf_test_raw = []
    if isinstance(tr_ref.stats.response, AttribDict):
        _, tf_ref = freqs_zpk(tr_ref.stats.response['zeros'],
                              tr_ref.stats.response['poles'],
                              tr_ref.stats.response['gain'] *
                              tr_ref.stats.response['sensitivity'],
                              worN=2*np.pi*freq)
    elif isinstance(tr_ref.stats.response, Response):
        tf_ref = \
            tr_ref.stats.response.get_evalresp_response_for_frequencies(freq)
    else:
        tf_ref = np.ones(freq.size)
    for i in range(0, len(fft_ref_seg)):
        ratio_spectrum = np.divide(fft_test_seg[i], fft_ref_seg[i])
        tf_seg = np.multiply(ratio_spectrum, tf_ref)
        tf_test_raw.append(tf_seg)
    tf_test_raw = np.asarray(tf_test_raw)
    tf_test_raw = np.transpose(tf_test_raw)
    return tf_test_raw, freq


def crosscalib(tr_ref, tr_test, smoothie, segment_length, perc_overlap,
               perc_taper):
    """
    Execute all the steps to obtain the figure of the measured instrumental
    response.
    """
    tf_test_raw, freq = compute_tf(tr_ref, tr_test, segment_length,
                                   perc_overlap, perc_taper)
    tf_test_exp = np.median(tf_test_raw, axis=1)
    tf_test_exp = frac_octave_smooth(smoothie, freq, tf_test_exp)
    return tf_test_exp, freq
