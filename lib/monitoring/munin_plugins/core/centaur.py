# -*- coding: utf-8 -*-
"""
Centaur monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""

import json
import logging
try:
    from urllib.error import URLError
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen, URLError


logger = logging.getLogger(__name__)


class CentaurThresholds:
    """
    Store Centaur warning and critical thresholds.
    """


class CentaurData:
    """
    Store Centaur status data
    """
    VOLTS = {
        'http://nmx.ca/05/units/volts': 1,
        'http://nmx.ca/05/units/millivolts': 1e3,
    }

    SECONDS = {
        'http://nmx.ca/05/units/seconds': 1,
        'http://nmx.ca/05/units/milliseconds': 1e3,
        'http://nmx.ca/05/units/microseconds': 1e6,
        'http://nmx.ca/05/units/nanoseconds': 1e9,
    }

    def __init__(self):
        self.packet_number = 'U'
        self.memory_size = 'U'
        self.memory_free = 'U'
        self.store_percentage_used = 'U'
        self.soh_voltage_1 = 'U'
        self.soh_voltage_2 = 'U'
        self.soh_voltage_3 = 'U'
        self.external_soh_voltage_1 = 'U'
        self.external_soh_voltage_2 = 'U'
        self.external_soh_voltage_3 = 'U'
        self.satellites_used = 'U'
        self.internal_free_space = 'U'
        self.os_free_space = 'U'
        self.removable_SD_free_space = 'U'
        self.voltage = 'U'
        self.current = 'U'
        self.temperature = 'U'
        self.dac_count = 'U'
        self.time_error = 'U'
        self.time_quality = 'U'
        self.time_uncertainty = 'U'

    def _parse_int(self, data, key, factor=1, divisor=1):
        try:
            return int(data[key]['value']) * factor / divisor
        except (KeyError, TypeError, ValueError):
            return 'U'

    def _parse_float(self, data, key, factor=1, divisor=1):
        try:
            return float(data[key]['value']) * factor / divisor
        except (KeyError, TypeError, ValueError):
            return 'U'

    def import_data(self, address, port):
        """
        Send sockets to the Centaur and import data into this object.
        """
        url = 'http://%s:%s/api/v1/instruments/soh' % (address, port)
        try:
            response = urlopen(url, timeout=15)
            encoding = response.info().get_param('charset') or 'utf-8'
            data = json.loads(response.read().decode(encoding))
        except URLError as e:
            logger.error(e)
            return

        data = list(data.values())[0]

        self.packet_number = self._parse_int(data, 'controller/numberPackets')
        self.memory_size = self._parse_int(data, 'controller/ppc/memory/total')
        self.memory_free = self._parse_int(data, 'controller/ppc/memory/free')
        self.store_percentage_used = \
            self._parse_float(data, 'controller/store/storePercentageUsed')
        self.soh_voltage_1 = \
            self._parse_float(data, 'digitizer/sensor/soh/voltage#_1')
        self.soh_voltage_2 = \
            self._parse_float(data, 'digitizer/sensor/soh/voltage#_2')
        self.soh_voltage_3 = \
            self._parse_float(data, 'digitizer/sensor/soh/voltage#_3')
        self.external_soh_voltage_1 = \
            self._parse_float(data, 'externalSoh/voltage#_1')
        self.external_soh_voltage_2 = \
            self._parse_float(data, 'externalSoh/voltage#_2')
        self.external_soh_voltage_3 = \
            self._parse_float(data, 'externalSoh/voltage#_3')
        self.satellites_used = self._parse_int(data, 'gps/numberOfSatellites')
        self.internal_free_space = \
            self._parse_int(data, 'media/freeSpace/internal')
        self.os_free_space = self._parse_int(data, 'media/freeSpace/os')
        self.removable_SD_free_space = \
            self._parse_int(data, 'media/freeSpace/removableSD')
        self.voltage = self._parse_float(data, 'powerSupply/voltage')
        self.current = self._parse_float(data, 'system/current', factor=1e3)
        self.temperature = self._parse_float(data, 'temperature')
        self.dac_count = self._parse_int(data, 'timing/dacCount')
        self.time_error = self._parse_float(data, 'timing/timeError')
        self.time_quality = self._parse_float(data, 'timing/timeQuality')
        self.time_uncertainty = \
            self._parse_float(data, 'timing/timeUncertainty')
