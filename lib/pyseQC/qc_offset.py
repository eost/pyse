#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np


def _qc_offset(offset_list):

    offset_array = np.array(offset_list)
    offset_array = offset_array.flatten()

    if len(offset_array) > 0:
        print("Digitizers offsets statistics:")
        print("Median offset: %d counts" % (np.median(offset_array)))
        tile75 = np.percentile(offset_array, 75)
        tile25 = np.percentile(offset_array, 25)
        print("Interquartile distance: %d counts" % (tile75-tile25))
        print("Max offset: +/- %.3f counts" %
              (np.abs(offset_array).max()))
        print("")

        fo = pyplot.figure('digitizers offsets', dpi=150)
        ax = fo.gca()
        ax.plot(offset_array)
        ax.hlines(np.median(offset_array), 0, offset_array.size, 'r',
                  label='Median')
        ax.legend(loc='best')
        ax.grid()
        ax.set_ylabel("Counts")
        fo.suptitle("Digitizers offsets")

        return fo
