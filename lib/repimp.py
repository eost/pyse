# -*- coding: utf-8 -*-
"""
Routine for relative calibration of broadband seismometers, made by the method
of impulse response fitting.
Theory:

    Reference equation:  ü + 2hw°u' +w°²u = f

 1- The response of the seismometer to a speed Dirac is
    -w**2/(-w**2+2*j*h*w0*w+w0**2)
 2- For a transfer function, a division by w corresponds to a derivation of the
    signal, i.e to switch from displacement to speed, divide the function by w.
 3- Establish (cut) a constant current in the test coil equals to imposing
    (stopping) a constant force, thus impose a acceleration Heaviside, thus a
    derivative acceleration Dirac.
 4- From 1 and 2, a derivative of acceleration on a broadband sensor produces a
    signal whose spectrum is -1/(-w**2+2*j*h*w0*w+w0**2), dividing twice by w
    to switch from speed to derivative of acceleration.
 5- The corresponding time signal from such a spectrum is
    f(t)= exp(-h*w0*t).sin(w0*t*sqrt(1-h**2))/(w0*sqrt(1-h**2))

             hence this modelisation:

    w=2*pi/period
    w1=w*sqrt(1-h**2)
    f(x)=offset+(x>x0)*a*exp(-h*w*(x-x0))*sin(w*sqrt(1-h**2)*(x-xo)/per)
    amp=a*w1

:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr) and Jean-Jacques Leveque
    (leveque@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from pyse_util import rms


def _model(x, x0, p, h, a, offset):
    """
    Function returning a theorical single impulse response in time domain \
    according to the model:
    f(x)=offset+(x>x0)*a*exp(-h*w*(x-x0))*sin(w*sqrt(1-h**2)*(x-xo)/per)
    Used for defining best fit by curve_fit.

    :type x: array
    :param x: Time array.
    :type x0: int
    :param x0: Indice of time where pulse begins. Should be <len(x).
    :type p: int
    :param p: Theorical period.
    :type h: float
    :param h: Theorical damping.
    :type a: float
    :param a: Theorical amplitude of pulse. Should always be equal to 1.
    :type offset: float
    :param offset: Theorical offset. Should always be equal to 0.
    :returns: Array of theorical impulse response in time domain.
    """
    w = 2*np.pi/p
    cond = [x > x0, ]
    X = np.select(cond, [x])
    X = np.equal(X, x)
    X[0] = False
    t = x-x0
    if h < 1:
        resp = offset + X*a*np.exp(-h*w*t)*np.sin(w*t*np.sqrt(1-h**2))
    elif h == 1:
        resp = offset + X*a*t*np.exp(-h*w*t)
    elif h > 1:
        resp = offset + X*a*(np.exp(-w*t*(h-np.sqrt(h**2-1))) -
                             np.exp(-w*t*(h+np.sqrt(h**2-1))))
    return resp


def repimp(response_trace, **kwargs):
    """
    Function for period and damping calculation from several impulse responses.

    :param response_trace: Trace object from module obspy.core.trace \
    corresponding to the response of a coil (U, V, W) in velocity. It must \
    have a sampling rate equal or higher than 20Hz and must begin by at least \
    1 min of  noise.
    :type verbose: bool
    :param verbose: Specify if program gives a result a each pulse (True) or \
    not (False) (default: False).
    :type plotting: bool
    :param plotting: Specify if program shows the overall results in a figure \
    (True) or not (False) (default: False).
    :type synchro_coils: bool
    :param synchro_coils: Specify if signal has been injected in each coil at \
    a time (True) or in every coils together (False) (default: True).
    :type period: int
    :param period: Specify expected period. Use this option if model does'nt \
    work (default: 120).
    :returns: average period, period deviation, average damping, damping \
    deviation, average signal to noise ratio. All variable are calculated from\
    falling edge for better results.
    """
    sr = response_trace.stats.sampling_rate
    # verify if sampling rate is high enough
    if (sr < 20):
        print("[lib.repimp]: Sampling rate too low")
        raise SystemExit

    # demean trace: means offset is not taken into account.
    response_trace.detrend('demean')

    # give pre-defined (ie. before fitting) period
    if 'period' in kwargs:
        _period = kwargs['period']
    else:
        _period = 120

    # rough determination of first pulse with threshold at 0.3 of max(absolute)
    # and of number of usable pulses.
    threshold = np.absolute(response_trace.max())*0.3
    nb_cycles = 0
    i_pulse_first = 0
    i_pulse_last = 0
    pulse = False
    sel = np.select([np.abs(response_trace.data) < threshold,
                     np.abs(response_trace.data) >= threshold],
                    [np.zeros(response_trace.stats.npts),
                     np.ones(response_trace.stats.npts)])
    sel = np.convolve(sel, np.ones(2*int(sr)))
    for i in range(0, sel.size):
        if sel[i] > 0 and not pulse:
            pulse = True
            nb_cycles +=1
            i_pulse_last = i
            if i_pulse_first == 0:
                i_pulse_first = i
        elif sel[i] == 0 and pulse:
            pulse = False

    if nb_cycles % 2 == 0:
        nb_cycles = nb_cycles // 2
    else:
        print("[lib.repimp]: At least one cycle is not complete")
        raise SystemExit
    print("%s: %s pulses" % (response_trace.id, str(nb_cycles)))

    # determination of one pulse duration by averaging
    if 'synchro_coils' in kwargs and not kwargs['synchro_coils']:
        duration = (i_pulse_last-i_pulse_first) // ((nb_cycles-1)*6+1)
    else:
        duration = (i_pulse_last-i_pulse_first) // ((nb_cycles-1)*2+1)
    duration_sec = duration/sr
    if 'verbose' in kwargs and kwargs['verbose']:
        print(response_trace.id + " pulse duration : " +
              str(duration_sec)+" s.")

    # define a margin for pulse determination (15s)
    if 'margin' in kwargs:
        margin_sec = kwargs['margin']
    else:
        margin_sec = 15
    margin_ind = int(margin_sec*sr)

    # creation of figure
    if 'plotting' in kwargs and kwargs['plotting']:
        _fig = plt.figure()
        _ax_spikes = _fig.add_axes([0.02, 0.52, 0.96, 0.43])
        _ax_spikes.set_title("stacked spikes")
        _ax_spikes.set_yticks([])
        _ax_spikes.set_xticks([])
        _ax_per_in = _fig.add_axes([0.02, 0.29, 0.44, 0.21])
        _ax_per_in.set_yticks([])
        _ax_per_in.set_xticks([])
        _ax_per_in.set_xlabel("injection")
        _ax_per_re = _fig.add_axes([0.54, 0.29, 0.44, 0.21])
        _ax_per_re.set_ylabel("period")
        _ax_per_re.set_yticks([])
        _ax_per_re.set_xticks([])
        _ax_per_re.set_xlabel("rest")
        _ax_dam_in = _fig.add_axes([0.02, 0.02, 0.44, 0.21])
        _ax_dam_in.set_yticks([])
        _ax_dam_in.set_xticks([])
        _ax_dam_re = _fig.add_axes([0.54, 0.02, 0.44, 0.21])
        _ax_dam_re.set_ylabel("damping")
        _ax_dam_re.set_yticks([])
        _ax_dam_re.set_xticks([])

    # sort pulses by type (pos or neg) and plot them stacked for check
    for i in range(0, nb_cycles):

        if 'synchro_coils' in kwargs and not kwargs['synchro_coils']:
            index = i_pulse_first+i*duration*6
        else:
            index = i_pulse_first+i*duration*2

        if 'plotting' in kwargs and kwargs['plotting']:
            _ax_spikes.plot(response_trace.data[index-margin_ind:index +
                            duration-margin_ind])
            _ax_spikes.plot(response_trace.data[index + duration -
                            margin_ind:index+2*duration-margin_ind])

    # results calculation pulse by pulse
    periods = np.array([])
    dampings = np.array([])
    snr = np.array([])
    for i in range(0, nb_cycles):

        if 'synchro_coils' in kwargs and not kwargs['synchro_coils']:
            index = i_pulse_first + i*duration*6
        else:
            index = i_pulse_first + i*duration*2

        if 'period' in kwargs:
            _period = kwargs['period']
        else:
            _period = 120

        for t0 in ([response_trace.stats.starttime + index/sr,
                   response_trace.stats.starttime + index/sr + duration/sr]):
            tr_calc = response_trace.slice(starttime=t0 - margin_sec,
                                           endtime=t0 - margin_sec +
                                           duration_sec)
            noise_trace = tr_calc.copy()
            if np.mean(tr_calc.data) < 0:
                tr_calc.data = tr_calc.data*-1.0

            # first run of fitting function, suggesting standard values
            t = np.arange(tr_calc.stats.npts)
            (_popt, pcov) = curve_fit(_model, t, tr_calc.data,
                                      p0=[0, _period*sr, 0.707, 1, 0],
                                      maxfev=int(1e6))

            # withdrawal of offset and noise before pulse, the second run by \
            # suggesting results of first run
            tr_calc.data = tr_calc.data[int(_popt[0]):]
            tr_calc.data = tr_calc.data-_popt[4]
            t = np.arange(tr_calc.stats.npts)
            (popt, pcov) = curve_fit(_model, t, tr_calc.data,
                                     p0=[0, _popt[1], _popt[2], _popt[3], 0],
                                     maxfev=int(1e6))

            PERIOD = popt[1]/sr
            DAMPING = popt[2]
            noise_trace.data = tr_calc.data-_model(t, popt[0], popt[1],
                                                   popt[2], popt[3], popt[4])
            SNR = rms(response_trace)/rms(noise_trace)

            if 'verbose' in kwargs and kwargs['verbose']:
                print("%s cycle #%s: %s %s %s" % (response_trace.id, str(i),
                                                  format(PERIOD, ".3f"),
                                                  format(DAMPING, ".6f"),
                                                  format(SNR, ".3f")))

            periods = np.append(periods, PERIOD)
            dampings = np.append(dampings, DAMPING)
            snr = np.append(snr, SNR)

    if 'keep_all_pulses' not in kwargs or kwargs['keep_all_pulses'] is False:
        periods = periods[1::2]
        dampings = dampings[1::2]

    rest_periods = np.mean(periods)
    per_std = np.std(periods)
    rest_dampings = np.mean(dampings)
    damp_std = np.std(dampings)
    rest_snr = np.mean(snr)

    # plot results with 5% limits
    if 'plotting' in kwargs and kwargs['plotting']:

        pre = 0.05

        _ax_per_in.plot(periods, '*')
        _ax_per_in.axhline(_period-_period*pre, linewidth=2, color='r')
        _ax_per_in.axhline(_period+_period*pre, linewidth=2, color='r')

        _ax_per_re.plot(periods, '*')
        _ax_per_re.axhline(_period-_period*pre, linewidth=2, color='r')
        _ax_per_re.axhline(_period+_period*pre, linewidth=2, color='r')

        _ax_dam_in.plot(dampings, '*')
        _ax_dam_in.axhline(0.707-0.707*pre, linewidth=2, color='r')
        _ax_dam_in.axhline(0.707+0.707*pre, linewidth=2, color='r')

        _ax_dam_re.plot(dampings, '*')
        _ax_dam_re.axhline(0.707-0.707*pre, linewidth=2, color='r')
        _ax_dam_re.axhline(0.707+0.707*pre, linewidth=2, color='r')

        plt.show()

    return (rest_periods, per_std, rest_dampings, damp_std, rest_snr)
