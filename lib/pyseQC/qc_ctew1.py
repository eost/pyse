#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np
from obspy.core import UTCDateTime
from obspy.signal.invsim import corn_freq_2_paz

from pyCTEW1 import HCalc
from pyCTEW1 import VCalc


def _qc_ctew1(ctew1_requests, server, conf):

    if len(ctew1_requests) > 0:
        V_gain = np.array([])
        E_gain = np.array([])
        N_gain = np.array([])
        error = conf['main']['error']

        i = 0
        for request in ctew1_requests:
            print("%d %s" % (i // 3, request))
            i += 1
            words = request.split(' ')
            t1 = UTCDateTime(words[2])
            t2 = UTCDateTime(words[3])
            stream = words[4]
            seed_codes = stream.split('.')
            try:
                st = server.get_waveforms(seed_codes[0], seed_codes[1],
                                          seed_codes[2], seed_codes[3], t1, t2)

                st.trim(t1, t2)
                tr = st[0]
                _paz = corn_freq_2_paz(1./conf['main']['sensor_period'],
                                       damp=conf['main']['sensor_damping'])
                lsb = 1./conf['main']['digitizer_gain']
                for word in words:
                    if word[:5] == '--lsb':
                        lsb = float(word.split('=')[-1])
                if lsb == 1./conf['main']['digitizer_gain']:
                    print("%s: lsb not taken in account" % (request))
                _paz['sensitivity'] = 1./lsb

                if words[0] == 'ctew1_V_analyzer':
                    if seed_codes[3] != 'HHZ':
                        print("%s: bad sampling rate, use HHZ" % (request))

                    (G, D, S) = VCalc(tr, _paz,
                                      conf['qc_ctew1']['vertical_displacement'],
                                      conf['qc_ctew1']['voltage_drop'],
                                      verbose=False, plotting=False,
                                      saving=False)

                elif words[0] == 'ctew1_H_analyzer':
                    if seed_codes[3][:2] != 'BH':
                        print("%s: bad sampling rate, use BH" % (request))

                    (G, D, S) = HCalc(tr, _paz,
                                      conf['qc_ctew1']['horizontal_displacement'],
                                      conf['qc_ctew1']['voltage_drop'],
                                      conf['qc_ctew1']['table_length'],
                                      conf['qc_ctew1']['gravity'],
                                      verbose=False, plotting=False,
                                      saving=False)

                if G > conf['main']['sensor_gain']*(1+error) or\
                   G < conf['main']['sensor_gain']*(1-error):
                    print("Result out of range")
                else:
                    if seed_codes[3] == 'BHE':
                        E_gain = np.append(E_gain, np.array([G, D, S]))

                    elif seed_codes[3] == 'BHN':
                        N_gain = np.append(N_gain, np.array([G, D, S]))

                    elif seed_codes[3] == 'HHZ':
                        V_gain = np.append(V_gain, np.array([G, D, S]))

            except Exception as e:
                print(e)

        if len(V_gain) == len(E_gain) and len(V_gain) == len(N_gain) and\
           len(V_gain) > 0:

            print("Ctew1 east component statistics:")
            print("Average sensitivity: %.1f Vs/m" % (E_gain[::3].mean()))
            print("Standard deviation of sensitivities: %.1f Vs/m" %
                  (E_gain[::3].std()))
            print("Max error from theorical sensitivity: %.3f percent" %
                  (np.abs(1 - E_gain[::3]/conf['main']['sensor_gain']).max()*100))
            print("Max error from average sensitivity: %.3f percent" %
                  (np.abs(1 - E_gain[::3]/E_gain[::3].mean()).max()*100))

            print("Ctew1 north component statistics:")
            print("Average sensitivity: %.1f Vs/m" % (N_gain[::3].mean()))
            print("Standard deviation of sensitivities: %.1f Vs/m" %
                  (N_gain[::3].std()))
            print("Max error from theorical sensitivity: %.3f percent" %
                  (np.abs(1 - N_gain[::3]/conf['main']['sensor_gain']).max()*100))
            print("Max error from average sensitivity: %.3f percent" %
                  (np.abs(1 - N_gain[::3]/N_gain[::3].mean()).max()*100))

            print("Ctew1 vertical component statistics:")
            print("Average sensitivity: %.1f Vs/m" % (V_gain[::3].mean()))
            print("Standard deviation of sensitivities: %.1f Vs/m" %
                  (V_gain[::3].std()))
            print("Max error from theorical sensitivity: %.3f percent" %
                  (np.abs(1 - V_gain[::3]/conf['main']['sensor_gain']).max()*100))
            print("Max error from average sensitivity: %.3f percent" %
                  (np.abs(1 - V_gain[::3]/V_gain[::3].mean()).max()*100))
            print("")

            fc = pyplot.figure('ctew1', dpi=150)
            ax = fc.gca()
            x = np.arange(1, 1 + len(V_gain[::3]))

            ax.errorbar(x, E_gain[::3], yerr=E_gain[1::3], fmt='.b',
                        ecolor='b', label='East')
            ax.plot(x, E_gain[::3].mean()*np.ones(len(x)), ':b')
            ax.annotate("%.1f" % (E_gain[::3].mean()),
                        xy=(x[0], E_gain[::3].mean()),
                        xytext=(x[0], E_gain[::3].mean()))

            ax.errorbar(x, N_gain[::3], yerr=N_gain[1::3], fmt='.g',
                        ecolor='g', label='North')
            ax.plot(x, N_gain[::3].mean()*np.ones(len(x)), ':g')
            ax.annotate("%.1f" % (N_gain[::3].mean()),
                        xy=(x[0], N_gain[::3].mean()),
                        xytext=(x[0], N_gain[::3].mean()))

            ax.errorbar(x, V_gain[::3], yerr=V_gain[1::3], fmt='.r',
                        ecolor='r', label='Vert.')
            ax.plot(x, V_gain[::3].mean()*np.ones(len(x)), ':r')
            ax.annotate("%.1f" % (V_gain[::3].mean()),
                        xy=(x[0], V_gain[::3].mean()),
                        xytext=(x[0], V_gain[::3].mean()))

            ax.set_xlim(0, 1 + len(x))
            ax.grid()
            ax.legend(loc='best')
            ax.set_ylabel("Vs/m")

            fc.suptitle("Sensors sensitivities (calib. table)")

            return fc
