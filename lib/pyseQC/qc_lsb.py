#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np


def _qc_lsb(lsb_list, conf):

    lsb_array = np.array(lsb_list)

    if len(lsb_array) > 0:
        med = np.median(lsb_array)
        tile25 = np.percentile(lsb_array, 25)
        tile75 = np.percentile(lsb_array, 75)
        print("Digitizers LSBs statistics:")
        print("Median LSB: %d counts/V" % (med))
        print("Interquartile distance: %d counts/V" % (tile75-tile25))
        print("Max distance of LSBs from theory: %.3f percent" %
              (np.abs(lsb_array-conf['main']['digitizer_gain']).max() *
               100/conf['main']['digitizer_gain']))
        print("Max distance of LSBs from median: %.3f percent" %
              (np.abs(lsb_array-med).max()*100/med))
        print("")

        fl = pyplot.figure('digitizers sensititvities', dpi=150)
        ax = fl.gca()
        ax.plot(lsb_array)
        ax.hlines(np.median(lsb_array), 0, lsb_array.size, 'r',
                  label='Median')
        ax.hlines(conf['main']['digitizer_gain'], 0, lsb_array.size, 'r',
                  alpha=0.4, label='Theory')
        ax.legend(loc='best')
        ax.grid()
        yticks = ax.get_yticks()
        tick_delta = yticks[1] - yticks[0]
        ax.set_yticks(np.arange(yticks[0]-tick_delta, yticks[-1]+tick_delta,
                                tick_delta))
        ax.set_ylabel("Counts/V")
        fl.suptitle("Digitizers sensitivities")

        return fl
