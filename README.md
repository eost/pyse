## Description:

The pyse toolkit provides all libraries, algorithms and executables needed at PISE (Plateforme Instrumentale Sismologique de l'EOST, [https://eost.unistra.fr/plateformes/pise](https://eost.unistra.fr/plateformes/pise))

## Installation:

Move into your installation directory (later called <pyse_dir>) and clone the toolkit from gitlab:
```
$ git clone https://gitlab.com/eost/pyse
```
Edit your .bashrc file and fill it that way:
> export PYSE_ROOT=<pyse_dir>/pyse
> export PATH=$PYSE_ROOT/bin:$PATH
> export PYTHONPATH=$PYSE_ROOT/lib:$PYTHONPATH

The use of package installer pip require a virtual environment since latest version of python3.
```
$ python3 -m venv pysenv
$ source pysenv/bin/activate
$ (pysenv)$ python3 -m pip install -r <pyse_dir>/requirements.txt
```

## Configuration

Most of scripts get data from EOST dataselect webservice. By default, url is set to [http://dataselect.u-strasbg.fr:8080](http://dataselect.u-strasbg.fr:8080). Change by your own url <your_dataselect> with sed:
```
(pysenv) $ sed -i 's\dataselect.u-strasbg.fr:8080\<your_dataselet>\g' <pyse_dir>/bin/*
```
Furthermore, all scripts use docopt as arguments parser ([http://docopt.org/](http://docopt.org/)). To change default behaviour in any script, edit it and find default tag in file header:
> [default: 4.804e8].

## Test

Run the testing script:

```
(pysenv)$ PYSE_ROOT/test_pyse.run

```