#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bes de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

Usage:
    selfnoise_modelizer <path> --oc-sens=<oc-sens> --fc=<fc> \
--damping=<damping> [--fmin=<fmin>] [--fmax=<fmax>] [--smoothie=<smoothie>]


Options:
    -h --help               Show this screen.
    --version               Show version.
    <path>                  Path to results tree as done by \
selfnoise_estimator.
    --oc-sens=<oc_sens>     Open-circuit sensitivity in V.s/m, as models \
output are in Volts.
    --fc=<fc>               Sensor cutoff frequency (Hz).
    --damping=<damping>     Sensor overall damping.
    --fmin=<fmin>           Set minimum frequency to apply best fit \
[default: 0.01].
    --fmax=<fmax>           Set maximum frequency to apply best fit \
[default: 40].
    --smoothie=<smoothie>   Defines n for 1/n octave smooth applied on final \
result [default: 6].
"""
from docopt import docopt
import glob
from matplotlib import pyplot
import numpy as np
import os
import pandas as pd
from scipy.constants import Boltzmann, pi
from scipy.optimize import curve_fit
from scipy.signal import freqs_zpk

from pyse_util import frac_octave_smooth


def _calc_pz(fc, h):
    """
    Calculate poles and zeros from a cutoff frequency and a damping. Unlike
    corn_freq_2_paz from obspy, it returns a result if damping > 1.
    """
    w0 = 2*pi*fc
    zeros = np.array([0, 0])
    if h < 1:
        p1 = -1*h*w0 - 1j*w0*np.sqrt(1-h**2)
        p2 = -1*h*w0 + 1j*w0*np.sqrt(1-h**2)
    elif h >= 1:
        p1 = -1*h*w0 - w0*np.sqrt(h**2-1)
        p2 = -1*h*w0 + w0*np.sqrt(h**2-1)
    poles = np.array([p1, p2])
    return (zeros, poles)


def _csvfile2arrays(file):
    """
    Read csv file as computed by crosscalib.
    """
    my_csv = pd.read_csv(file, sep=',', header=0)
    freq = np.array(my_csv.frequency)
    selfnoise = np.array([complex(i) for i in my_csv.noise])
    return (freq, selfnoise)


def _impedance_model(params, R):
    """
    Compute best selfnoise model with cutoff frequency and damping being known.
    Calls _selfnoise_model.
    """
    f = params[:-2]
    fc = params[-2]
    h = params[-1]
    return _selfnoise_model(f, fc, h, h, R)


def _mecdamp_model(params, h0):
    """
    Compute best selfnoise model with cutoff frequency, damping and equivalent
    impedance being known. Calls _selfnoise_model.
    """
    f = params[:-3]
    fc = params[-3]
    h = params[-2]
    R = params[-1]
    return _selfnoise_model(f, fc, h, h0, R)


def _selfnoise_model(frequency, fc, h, h0, R):
    """
    Compute selfnoise model from fc (cutoff frequency), h (overall damping),
    h0 (mechanical damping) and R (equivalent impedance of sensor and analog
    stage). The model output is a PSD in V**2/Hz.
    """
    w = 2*pi*frequency
    w0 = 2*pi*fc
    T = 293.15
    sn = 4 * Boltzmann * T * R * (w0**2 - w**2 + 2j*h*w0*w) /\
        (w0**2 - w**2 + 2j*h0*w0*w)
    zeros, poles = _calc_pz(fc, h)
    _, tf = freqs_zpk(zeros, poles, 1, worN=w)
    sn = np.divide(sn, tf**2/w**2)
    return np.abs(sn)


if __name__ == '__main__':
    args = docopt(__doc__, version='selfnoise_modelizer 0.1')
    # Uncomment for debug
    # print(args)

    # Retrieve overall results
    csvfiles = glob.glob("%s/*_selfnoise.csv" % args['<path>'])
    sn = list()
    for file in csvfiles:
        (frequency, _sn) = _csvfile2arrays(file)
        sn.append(_sn)

    # Write median result
    mediansn = frac_octave_smooth(int(args['--smoothie']), frequency,
                                  np.median(sn, axis=0))
    med_dFrame = pd.DataFrame(data={'frequency': frequency,
                                    'noise': mediansn})
    med_dFrame.to_csv(os.path.join(args['<path>'], 'selfnoise_median.csv'),
                      index=False)
    # Select frequencies range for best curve fit
    min_indx = 0
    max_indx = len(frequency)
    fmin = float(args['--fmin'])
    fmax = float(args['--fmax'])
    if args['--fmin']:
        min_indx = np.argmin(np.abs(frequency-fmin))
    if args['--fmax']:
        max_indx = np.argmin(np.abs(frequency-fmax))

    # Parse arguments
    fc = float(args['--fc'])
    h = float(args['--damping'])
    oc_sens = float(args['--oc-sens'])
    # Prepare params for impedance model and curve fit
    params = np.append(frequency[min_indx:max_indx], [fc, h])
    popt, pcov = curve_fit(_impedance_model, params,
                           np.abs(mediansn[min_indx:max_indx]*oc_sens))
    R = int(popt[0])

    # Prepare params for mechanical damping model and curve fit
    params = np.append(frequency[min_indx:max_indx], [fc, h, R])
    popt, pcov = curve_fit(_mecdamp_model, params,
                           np.abs(mediansn[min_indx:max_indx]*oc_sens))
    h0 = popt[0]

    # Compute full model and write it to csv file in acceleration
    modelsn = _selfnoise_model(frequency, fc, h, h0, R)
    mod_dFrame = pd.DataFrame(data={'frequency': frequency,
                                    # 'noise': np.divide(modelsn, oc_sens)})
                                    'noise': modelsn})
    mod_dFrame.to_csv(os.path.join(args['<path>'], 'selfnoise_model.csv'),
                      index=False)

    # Plot results (measurement and label)
    fig = pyplot.figure()
    ax = fig.gca()
    ax.semilogx(frequency, 10*np.log10(np.abs(mediansn)), label='measurement')
    ax.semilogx(frequency, 10*np.log10(np.abs(modelsn) / oc_sens),
                label='model')
    ax.set_xlabel("Frequency  [Hz]")
    ax.set_ylabel("Selfnoise PSD  [m**2/(s**4*Hz)]")
    ax.grid()
    ax.legend()
    pyplot.show()
    fig.savefig(os.path.join(args['<path>'], 'best_fit.png'))

    # Print results:
    print("Best fit computed between {} and {} Hz" .format(fmin, fmax))
    print("Equivalent impedance of sensor and analog stage: {} Ohms".format(R))
    print("Mechanical damping: {}".format(h0))
