#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

 Usage:
    timeshift_analyzer fdsn <start> <end> <stream> [--url=<url>] \
[--ref-stream=<ref-stream>] [--fmin=<fmin>]  [--fmax=<fmax>] [--mode=<mode>] \
[--step=<step>] [--bulk=<bulk>]
    timeshift_analyzer mseed <test_file> <ref_file> [--fmin=<fmin>] \
[--fmax=<fmax>] [--mode=<mode>] [--step=<step>] [--bulk=<bulk>]

Example:
    timeshift_analyzer fdsn 2016-09-30T00:00:00 2016-09-30T05:00:00 \
XX.GPIL.00.HHZ --fmin=0.1 --fmax=10

Options:
    -h --help           Show this screen.
    --version           Show version.
    <start>             Start time in obspy.core.UTCDateTime format.
    <end>               End time in obspy.core.UTCDateTime format.
    <stream>            Stream to analyse in seed code separated by dots. \
(Ex: XX.GPIL.00.HHZ).
    --url=<url>         Set base url for fdsnws dataselect \
[default: http://dataselect.u-strasbg.fr:8080]
    --ref-stream=<ref-stream>   Set reference stream [default: XX.GPIL.00.HHZ].
    --fmin=<fmin>       Set minimum frequency for bandpass filter \
[default: 0.1].
    --fmax=<fmax>       Set maximum frequency for bandpass filter \
[default: 10].
    --mode=<mode>       Set mode: 'full' means a correlation is made on all \
data. 'split' means a correlation is made every 'step' seconds on a period of \
'bulk' seconds, and the program returns an evolution of the timeshift between \
data [default: full].
    --step=<step>       Set 'step' in mode 'split' [default: 600].
    --bulk=<bulk>       Set 'bulk' in mode 'split' [default: 60].
"""

from docopt import docopt
from obspy.clients.fdsn import Client as fClient
from obspy.core import read, UTCDateTime
from matplotlib import pyplot
import numpy as np


def _corr(st, st_ref):
    for m in [st, st_ref]:
        m.detrend('demean')
        m.detrend('linear')
        m.taper(0.2)
        m.filter('bandpass', freqmin=float(args['--fmin']),
                 freqmax=float(args['--fmax']), zerophase=True, corners=8)
    C = np.correlate(st[0].data, st_ref[0].data, mode='full')
    delta = (np.argmax(np.abs(C))-st_ref[0].stats.npts+1) / \
        st_ref[0].stats.sampling_rate
    return delta, C


if __name__ == '__main__':
    args = docopt(__doc__, version='timeshift_analyzer 1.0')
    # Uncomment for debug
    # print(args)

    if args['fdsn']:
        data = fClient(base_url=args['--url'],
                       service_mappings={'event': None})
        t1 = UTCDateTime(args['<start>'])
        t2 = UTCDateTime(args['<end>'])
        code = args['<stream>'].split('.')
        st = data.get_waveforms(code[0], code[1], code[2], code[3], t1, t2,
                                attach_response=True)

        code_ref = args['--ref-stream'].split('.')
        st_ref = data.get_waveforms(code_ref[0], code_ref[1], code_ref[2],
                                    code_ref[3], t1, t2,
                                    attach_response=True)

        for tr in (st+st_ref):
            try:
                tr.remove_response(pre_filt=(0.001, 0.002,
                                             tr.stats.sampling_rate*0.4,
                                             tr.stats.sampling_rate*0.5))
            except AttributeError:
                pass

        st.trim(t1, t2)
        st_ref.trim(t1, t2)

    elif args['mseed']:
        st = read(args['<test_file>'])
        st_ref = read(args['<ref_file>'])

    (st+st_ref).sort()
    print((st + st_ref))
    (st + st_ref).plot(equal_scale=False)

    if args['--mode'] == 'full':
        delta, C = _corr(st, st_ref)
        print("Time shift in seconds: %f" % (delta))
        pyplot.plot(C)

    elif args['--mode'] == 'split':
        t1 = st[0].stats.starttime
        t2 = st[0].stats.endtime
        t = t1
        bulk = int(args['--bulk'])
        step = int(args['--step'])
        delta_array = np.array([])
        while t+step < t2:
            st_tmp = st.slice(t, t+bulk)
            st_ref_tmp = st_ref.slice(t, t+bulk)
            delta, C = _corr(st_tmp, st_ref_tmp)
            print("%s: Time shift of %f seconds" %
                  (st_tmp[0].stats.starttime, delta))
            delta_array = np.append(delta_array, delta)
            t += step
        pyplot.plot(delta_array)

    pyplot.show()
