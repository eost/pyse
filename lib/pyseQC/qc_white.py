#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np
from obspy.core import UTCDateTime
from scipy.signal import freqs, zpk2tf

from coilcalib import H_psd_div
from pyse_util import extract_paz_from_yamlcatalog


def _qc_white(white_requests, server, conf):

    if len(white_requests) > 0:
        frequencies = list()
        amplitudes = list()
        phases = list()
        error = list()
        paz = extract_paz_from_yamlcatalog(conf['qc_white']
                                               ['combined_resp_file'],
                                           conf['main']['sensor_model'])

        i = 0
        for request in white_requests:
            print("%d %s" % (i//3, request))
            i += 1
            words = request.split(' ')
            t1 = UTCDateTime(words[2])
            t2 = t1 + 600
            seed_codes = words[4].replace(words[4][-3:-1], 'HH').split('.')
            try:
                st = server.get_waveforms(seed_codes[0], seed_codes[1],
                                          seed_codes[2], seed_codes[3], t1, t2)
                st.trim(t1, t2)
                for word in words:
                    if word[:12] == '--ref-stream':
                        ref_stream = word.split('=')[1]
                if not ref_stream:
                    print("%s: no reference stream" % (request))
                else:
                    seed_codes =\
                        ref_stream.replace('BH', 'HH').split('.')
                    st_ref = server.get_waveforms(seed_codes[0],
                                                  seed_codes[1],
                                                  seed_codes[2],
                                                  seed_codes[3], t1, t2)
                    st_ref.trim(t1, t2)

                (f, A, P) = H_psd_div(st_ref, st,
                                      sm_n=conf['qc_white']['smooth'],
                                      fnorm=paz['normalization_frequency'],
                                      normalize=True)

                b, a = zpk2tf(paz['zeros'], paz['poles'], paz['gain'])
                _w, H0 = freqs(b, a, f * 2 * np.pi)
                Amp_th = np.absolute(H0)
                Pha_th = np.angle(H0, deg=True)

                F = np.ones(f.size, dtype=bool)
                cond = [f >= 1]
                F = np.select(cond, [F])
                cond = [f <= 40]
                F = np.select(cond, [F])
                cond = [np.absolute(20*np.log10(A/Amp_th)) > 20]
                O = np.select(cond, [F])

                if O.any():
                    print("Result out of range")
                else:
                    frequencies.append(f)
                    amplitudes.append(A)
                    phases.append(P)
                    error.append(np.abs(F*20*np.log10(A/Amp_th)).max())

            except Exception as e:
                print(e)

        if len(frequencies) == len(amplitudes) and\
           len(frequencies) == len(phases) and\
           len(frequencies) == len(error) and len(frequencies) > 0:

            frequencies = np.array(frequencies)
            amplitudes = np.array(amplitudes)
            phases = np.array(phases)
            error = np.array(error)

            print("White statistics:")
            print("Max error from theorical response: %.2f dB" % error.max())
            print("Avg. error from theorical response: %.2f dB" % error.mean())
            print("")

            fw = pyplot.figure('white', dpi=150)
            ax1 = fw.add_subplot(211)
            ax1.semilogx(f, 20*np.log10(amplitudes.mean(0)), 'g',
                         label='Average')
            ax1.semilogx(f, 20*np.log10(amplitudes[-1]), 'g', alpha=0.4,
                         label='Min/Max')
            ax1.semilogx(f, 20*np.log10(amplitudes.max(0)), 'g', alpha=0.4)
            ax1.grid()
            ax1.set_xlim((1, 90))
            ax1.set_ylim((-10, 10))
            ax1.set_ylabel("Amplitude (dB rel. to 1Vs/m)")
            ax2 = fw.add_subplot(212)
            ax2.semilogx(f, phases.mean(0), 'g')
            ax2.semilogx(f, phases.max(0), 'g', alpha=0.4)
            ax2.semilogx(f, phases.min(0), 'g', alpha=0.4)
            ax2.grid()
            ax2.set_xlim((1, 90))
            ax2.set_ylim((-180, 180))
            ax2.set_ylabel("Phase (deg))")
            ax2.set_xlabel("Frequency (Hz)")

            ax1.semilogx(f, 20*np.log10(Amp_th), ':r',
                         label='theorical')
            ax1.legend()
            ax2.semilogx(f, Pha_th, ':r')
            fw.suptitle("Sensors high frequencies transfer function")

            return fw
