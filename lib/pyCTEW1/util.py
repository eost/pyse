# -*- coding: utf-8 -*-

"""
Additional functions used by VCalc and HCalc

:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)

"""
import numpy as np
import warnings

from pyse_util import rms


def extractNoise(response_trace):
    """
    Function extracting noise from trace containing sequence. Sequence means \
    a square signal calculated by VCalc (displacement), or HCalc (acceleration)

    :type response_trace: trace object from obspy.core
    :param response_trace: trace whose noise has to be extracted
    :returns: array of indices where trace is noise and not part of sequence
    """

    # Copy original trace
    calc_trace = response_trace.copy()

    # Init parameters used in while loop
    i = 0
    # New array of empty indices
    bp = np.array([], dtype=int)
    # Threshold equals to rms
    seuil = rms(calc_trace)
    # No movement for now
    motionFree = True

    # Run on trace samples
    while (i < calc_trace.stats.npts):
        # If no movement
        if np.absolute(calc_trace.data[i]) < seuil:
            # But movement has juste ended
            if not motionFree:
                # Remove 0.3 seconds of data after
                i += int(0.3*calc_trace.stats.sampling_rate)
                motionFree = True
            # Otherwise append indice
            bp = np.append(bp, i)
        # If movement
        else:
            # But movement has just began
            if motionFree:
                for j in range(0, int(0.3*calc_trace.stats.sampling_rate)):
                    # Remove 0.3 seconds of data before
                    bp = np.delete(bp, -1)
                motionFree = False
        i += 1

    return bp


def bestFit(indices, trace):
    """
    Function extrapolating noise from trace containing sequence

    :type indices: numpy.array
    :param indices: indices of noise, as retrurned by extractNoise
    :type trace: trace object from obspy.core
    :param trace: trace whose noise has to be fitted
    :returns: array of extrapolated noise
    """
    deg = 1
    warnings.filterwarnings('error')
    # Initiate loop to choose best degree of polynomial extrapolation
    # Increment degree whille error occurs
    while True:
        try:
            best_fit = np.poly1d(np.polyfit(indices, trace.data[indices], deg))
            deg += 1
        except:
            break
    warnings.resetwarnings()

    # Make extrapolation at optimal degree
    x = range(0, trace.stats.npts)
    fit = best_fit(x)

    return fit
