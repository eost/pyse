# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""

from serial import Serial
import signal
from time import sleep
import numpy as np
from obspy.core import UTCDateTime, Trace


class DTM5080:

    def __init__(self, port, baud_rate, net, sta, loc, cha):
        self.ser = Serial(port, baud_rate)
        self.data = np.array([])
        self.trace = Trace()
        self.trace.stats.network = net
        self.trace.stats.station = sta
        self.trace.stats.location = loc
        self.trace.stats.channel = cha
        sleep(1)
        self.ser.write('t'.encode())
        sleep(0.1)
        self.ser.read(6)

    def signal_handler(self, signal, frame):
        self.npts = len(self.data)
        self.trace.data = self.data
        print(self.trace.stats)
        print(self.trace.stats.starttime.isoformat())
        self.trace.write(self.trace.stats.starttime.isoformat() + "." +
                         self.trace.id+".mseed", format='MSEED')
        exit()

    def acqui(self):
        signal.signal(signal.SIGINT, self.signal_handler)
        s = True
        while True:
            if self.ser.is_open:
                t = UTCDateTime.now()
                sleep(1-t.microsecond/1e6)
                self.ser.write('d'.encode())
                sleep(1e-3)
                t.second = (t.second+1) % 60
                t.microsecond = 0
                if s:
                    self.trace.stats.starttime = t
                    s = False
                T = float(self.ser.read(7)[:6])/100
                self.data = np.append(self.data, T)
                print("%s %s" % (t.isoformat(), str(T)))
            else:
                self.ser.open()
