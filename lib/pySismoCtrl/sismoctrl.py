# -*- coding: utf-8 -*-
"""
    Class for handling sismoCtrl (based on arduino) module. See \
    arduino/sismoCtrl/sismoCtrl.ino

:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)

:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr) and Jean-Jacques Leveque \
    (leveque@unistra.fr)

:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
import socket
from time import sleep


class Sismo:
    def __init__(self, IP, port, model):
        """
        Sismo constructor. Open network socket and test connection to \
        signal_injector module.

        :type IP: string
        :param IP: IP address. Must be compatible with the one specified in \
        arduino/sismoCtrl/sismoCtrl.ino
        :type port: int
        :param port: UDP port used by signal_injector module. Must be \
        compatible with the one specified in arduino/sismoCtrl/sismoCtrl.ino
        :type model: string
        :param model: Model of seimometer to control. Model must be compatible\
        with one available in arduino/sismoCtrl/sismoCtrl.ino
        """
        self.addr = (IP, port)
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.s.settimeout(3)

        if model == 't120':
            self.uvw_cmd = 'ctrl1_high\0'.encode()
            self.xyz_cmd = 'ctrl1_low\0'.encode()
            self.sp_cmd = 'ctrl2_high\0'.encode()
            self.lp_cmd = 'ctrl2_low\0'.encode()
            self.inj_cmd = 'cal_high\0'.encode()
            self.rest_cmd = 'cal_low\0'.encode()
        elif model == 'cmg40' or model == 'cmg40_low':
            self.uvw_cmd = 'ctrl3_low\0'.encode()
            self.xyz_cmd = 'ctrl3_high\0'.encode()
            self.sp_cmd = 'ctrl2_low\0'.encode()
            self.lp_cmd = 'ctrl2_high\0'.encode()
            self.inj_cmd = 'ctrl1_low\0'.encode()
            self.rest_cmd = 'ctrl1_high\0'.encode()
        else:
            print("[pyse.lib.pySismoCtrl.sismoctrl]: No seismometer specified")
            raise SystemExit

        self.xyz()
        self.lp()
        self.rest()

    def injection(self):
        """
        Commands signal injection start.
        """
        try:
            self.s.sendto(self.inj_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Connection lost")
            raise SystemExit

    def rest(self):
        """
        Commands signal injection stop.
        """
        try:
            self.s.sendto(self.rest_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Connection lost")
            raise SystemExit

    def center(self):
        """
        Commands recentering.
        """
        try:
            self.s.sendto(self.sp_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Error, connection lost")
            raise SystemExit
        sleep(3)
        try:
            self.s.sendto(self.lp_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Error, connection lost")
            raise SystemExit

    def sp(self):
        """
        Commands short period mode.
        """
        try:
            self.s.sendto(self.sp_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Error, connection lost")
            raise SystemExit

    def lp(self):
        """
        Commands long period mode.
        """
        try:
            self.s.sendto(self.lp_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Error, connection lost")
            raise SystemExit

    def uvw(self):
        """
        Commands uvw output mode.
        """
        try:
            self.s.sendto(self.uvw_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Error, connection lost")
            raise SystemExit

    def xyz(self):
        """
        Commands xyz output mode.
        """
        try:
            self.s.sendto(self.xyz_cmd, self.addr)
            if self.s.recv(4096) != "Done".encode():
                print("[pyse.lib.pySismoCtrl.sismoctrl]: Bad response")
        except socket.timeout:
            self.s.close()
            print("[pyse.lib.pySismoCtrl.sismoctrl]: Error, connection lost")
            raise SystemExit

    def close(self):
        """
        Close network socket.
        """
        self.s.close()
