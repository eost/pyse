/* bbb_plugin v0.2
Seiscomp acquisition plugin for BeagleBone/BeagleBoneBlack boards

This plugin needs:
libfb: http://www.freebasic-portal.de/downloads/fb-on-arm/bbb-fbc-fbc-fuer-beaglebone-black-283.html
libprussdrv: http://www.freebasic-portal.de//downloads/fb-on-arm/fb-prussdrv-kit-bbb-324.html
libpruio: http://users.freebasic-portal.de/tjf/Projekte/libpruio/doc/html/
plugin.h/plugin.c: provided within seiscomp3 package

Make sure user have permissions for read/write on /dev/uio5:
    groupadd pruio
    chgrp pruio /dev/uio5
    chmod g+rw /dev/uio5
    useradd -G pruio sysop

Maxime Bès de Berc 2014-10-01

v0.1: 1 instance per ADC
v0.2: 1 instance for all wanted ADCs -> best for BBB's resources

Compile by:
gcc -Wall -c plugin.c -o plugin.o
gcc -Wall -o /home/sysop/seiscomp3/share/plugins/seedlink/bbb_plugin plugin.o main.c -lpruio -L/usr/local/lib/freebasic -lfb -lpthread -lprussdrv -ltermcap -lsupc++ -lrt
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <pruio.h>

#include "plugin.h"

#define VERSION "0.2"

#define BUFSIZE 1024
#define MAX_STRING_SIZE 128

int acqFromADC(char *station, int mask, int sampling_rate);
int options(int argc, char *argv[], char *key_file);
int loadFromKey(FILE *key, char *station, char *mask, char *sampling_rate);

int main(int argc, char *argv[]){

    char station[6]="\0"; //station name cannot be more than 5 characters according to seed manual
    char mask[12]="\0"; //
    char sampling_rate[6]="\0"; //sampling_rate cannot contains more than 5 letters, ie Fe max= "99999" Hz
    char key_file[MAX_STRING_SIZE]="\0"; //key file path cannot be more than 128 characters, should be enough
    FILE *key;

    if(options(argc, argv, key_file)){
        exit(1);
    }

    key=fopen(key_file, "r");
    if(key==NULL){
         perror("[bbb_plugin]Bad key file specified");
         exit(1);
    }

    if(loadFromKey(key, station, mask, sampling_rate)){
        goto error;
    }

    if(acqFromADC(station, strtoul(mask, NULL, 16), atoi(sampling_rate))){
        goto error;
    }

    //End of program
    error:
	fclose(key);
        exit(1);
}

/*Acquisition function:
station: char pointer to a string containing the station name according to seed rules (ex: "TEST\0")
mask: integer representing which ADC is activated
    Example: for activating the three first ADCs, mask = 0x01F
sampling_rate: integer sampling rate in Hz. BBB rev C does not support more than 10000Hz.

Returns -1 in case of error, otherwise never returns and make acquisition
*/
int acqFromADC(char *station, int mask, int sampling_rate)
{
    int **buffers; //dynamic tab containing the samples, when full, samples are sent by send_raw_depoch
    int i, cursor; //indices to get into **buffers
    char *names;  //string containing all the ADCs activated: "012" means ADC0, ADC1 and ADC2 are activated
    char chan[2]="\0\0"; //string sent by send_raw_depoch as channel name, "0", "1", ..., "7" will be the channel name
    unsigned long delta;
    struct timespec *next, *firstSample;
    pruIo *io = pruio_new(PRUIO_ACT_ADC, 0, PRUIO_DEF_ODELAY, PRUIO_DEF_SDELAY);   // create new driver UDT
    if(pruio_config(io, PRUIO_DEF_SAMPLS, mask, PRUIO_DEF_TIMERV, 0)){
        perror("[bbb_plugin]Pru not correctly configured");
	pruio_destroy(io);
        return -1;
    }
    names=malloc(8*sizeof(char));
    strcpy(names,"");
    //Next sample's date allocation
    next=malloc(sizeof(struct timespec));
    //First sample's date allocation
    firstSample=malloc(sizeof(struct timespec));
    delta=(unsigned long)(1e9/sampling_rate);
    //Check mask
    if (mask & (1u << 1)){strcat(names, "0");}
    if (mask & (1u << 2)){strcat(names, "1");}
    if (mask & (1u << 3)){strcat(names, "2");}
    if (mask & (1u << 4)){strcat(names, "3");}
    if (mask & (1u << 5)){strcat(names, "4");}
    if (mask & (1u << 6)){strcat(names, "5");}
    if (mask & (1u << 7)){strcat(names, "6");}

    if (strlen(names)==0){
        fprintf(stderr, "[bbb_plugin]Bad mask, no channel selected\n");
        pruio_destroy(io);
        free(names);
        free(next);
        free(firstSample);
        return -1;
    }

    //Allocation buffers
    buffers=malloc(strlen(names)*sizeof(int*));
    for (i=0; i<strlen(names); i++){
        buffers[i]=malloc(BUFSIZE);
    }

    //Get current time
    if(clock_gettime(CLOCK_REALTIME, next)){
        perror("[bbb_plugin]acqFromADC->clock_gettime");
        goto error;
    }
    //Calculate next sample (also the first sample) date
    next->tv_sec++;
    next->tv_nsec=0;
    firstSample->tv_sec=next->tv_sec;
    firstSample->tv_nsec=next->tv_nsec;

    while(1){
        for(cursor=0; cursor*sizeof(int)<BUFSIZE; cursor++){

            //Wait for next sample date
            if(clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, next, NULL)){
               perror("[bbb_plugin]acqFromSource->clock_nanosleep");
               goto error;
            }
            //Record samples from all activated ADCs
            for (i=0; i<strlen(names); i++){
                buffers[i][cursor]=io->Adc->Value[i+1];
            }

            //Calculating next sample date
            if((next->tv_nsec+delta)%(unsigned long)1e9 < next->tv_nsec){
                next->tv_sec++;
            }
            next->tv_nsec=(next->tv_nsec+delta)%(unsigned long)1e9;

        }

        //Sending all buffers to seedlink server
        for (i=0; i<strlen(names); i++){
            chan[0]=*(names+i);
            if(send_raw_depoch(station, chan, (double)(firstSample->tv_sec+1e-9*firstSample->tv_nsec), 0, 100, buffers[i], cursor)!=BUFSIZE){
                perror("[bbb_plugin]acqFromSource->send_raw_depoch");
                goto error;
            }
        }
        //Calculating date of first sample of buffers
        firstSample->tv_nsec=next->tv_nsec;
        firstSample->tv_sec=next->tv_sec;
    }

    //End of function
    error:
        free(buffers);
        free(next);
        free(firstSample);
        free(names);
        pruio_destroy(io);                      // destroy driver structure
        return -1;
}

/*Options management function:
argc: integer containing the number of arguments passed to the program (from main)
argv: tab of strings of size argc containing all the arguments passed to the program (from main)
key_file: pointer to a string containing the path of the key file needed.
    This string is empty and options fills it with the path specifief as program argument

TODO: Verbose option

Returns 0 in case of succes or exit in case of error
*/
int options(int argc, char *argv[], char *key_file){
    int i;

    for (i=1; i<argc; i++) {
		if (strcmp(argv[i], "-V") == 0) {
	    		printf("bbb_plugin version: %s\n", VERSION);
	   	 	exit(0);
        	}
		else if (strcmp(argv[i], "-h") == 0) {
			printf("bbb_plugin version %s\n", VERSION);
			printf("Acquisition of BeagleBone/BeagleBoneBlack ADCs for seiscomp\n");
			printf("Usage: bbb_plugin [options] -f key_file\n");
			printf("where options are:\n");
			printf("-V report the program version\n");
			printf("-h shows this message\n");
			printf("-v be more verbose, not supported yet.\n");
			printf("-f specify the key file, required\n");
			printf("\n");
			printf("The key file must contains at least 3 lines beginning by:\n");
			printf("station=\n");
			printf("mask=\n");
			printf("rate=\n");
            		printf("If those lines are not specified in key file, the program won't acquire anything and send an error\n");
			printf("Station name must be at least 5 letters (according to Seed Manual), ex: station=TEST\n");
			printf("Mask must be an hexadecimal number specified with 5 characters, see libpruio documentation for signification, ex: mask=0x1FE\n");
			printf("Sampling rate must be specified with no more characters than 5, ie Fmax=99999Hz. BBB rev C dose not support more than 10000Hz, ex: rate=10000\n");
			exit(0);
        	}
		else if (strcmp(argv[i], "-f") == 0) {
            		if (argc>=i+2){
                    		if (strlen(strcpy(key_file, argv[i+1]))==0){
				        fprintf(stderr, "[bbb_plugin]Key file not specified, use -f path_to_file\n");
					return -1;
				}
            		}
			else {
			        fprintf(stderr, "[bbb_plugin]Key file not specified, use -f path_to_file\n");
                		return -1;
                	}
        	}
    }
    return 0;
}

/*Load acquisition parameters from key file:
key: file descriptor to the key file, already open
station: pointer to a string containing the station name, filled by function
mask: pointer to a string containing the ADCs mask, filled by function
sampling_rate: pointer to a string containing the sampling rate in Hz, filles by function

Returns 0 in case of success or -1 in case of error due to missing parameters
*/

int loadFromKey(FILE *key, char *station, char *mask, char *sampling_rate){
    char *tmp=malloc(MAX_STRING_SIZE);
    int flag=0;

    //station name, prefix: 'station=' (size=8)
    while (fgets(tmp, MAX_STRING_SIZE, key) != NULL){
        //station name, prefix: 'station=' (size=8)
        if(strncmp(tmp, "station=", 8)==0){
            strncat(station, tmp+8, strlen(tmp+8)-1);
            flag++;
        }
        //channel name, prefix: 'mask=' (size=5)
        else if(strncmp(tmp, "mask=", 5)==0){
            strncat(mask, tmp+5, strlen(tmp+5)-1);
            flag++;
        }
        //rate, prefix: 'rate=' (size=5)
        else if (strncmp(tmp, "rate=", 5)==0){
            strncat(sampling_rate, tmp+5, strlen(tmp+5)-1);
            flag++;
        }
    }
    free(tmp);
    if(flag!=3){
        fprintf(stderr, "[bbb_plugin]Missing parameter(s) in key file\n");
        return -1;
    }
    else {
        return 0;
    }
}
