# -*- coding: utf-8 -*-
"""
Q330 monitoring core support.

:author:
    EOST (École et Observatoire des Sciences de la Terre)
"""

import logging
import os
import time
import socket
from struct import unpack
import sys


logger = logging.getLogger(__name__)


class Q330Thresholds:
    """
    Store Q330 warning and critical thresholds.
    """
    def __init__(self):
        self.mass_warning = os.environ.get('mass.warning') or '-15:15'
        self.mass_critical = os.environ.get('mass.critical') or '-35:35'
        self.clock_quality_warning = \
            os.environ.get('clock_quality.warning') or '80:'
        self.clock_quality_critical = \
            os.environ.get('clock_quality.critical') or '60:'
        self.main_current_warning = \
            os.environ.get('main_current.warning') or 200
        self.main_current_critical = \
            os.environ.get('main_current.critical') or 500
        self.gps_current_warning = \
            os.environ.get('gps_antenna_current.warning') or 20
        self.gps_current_critical = \
            os.environ.get('gps_antenna_current.critical') or 25
        self.dp_memory_warning = os.environ.get('dp_memory.warning') or 5
        self.dp_memory_critical = os.environ.get('dp_memory.critical') or 80
        self.voltage_warning = os.environ.get('power.warning') or '12:'
        self.voltage_critical = os.environ.get('power.critical') or '11.5:'
        self.satellites_used_warning = \
            os.environ.get('satellites_used.warning') or '4:'
        self.satellites_used_critical = \
            os.environ.get('satellites_used.critical') or '2:'
        self.temperature_warning = os.environ.get('temperature.warning') or 45
        self.temperature_critical = \
            os.environ.get('temperature.critical') or 50
        self.uptime_warning = os.environ.get('uptime.warning') or '7:'
        self.uptime_critical = os.environ.get('uptime.critical') or '1:'


class Q330Data:
    """
    Store Q330 status data
    """
    def __init__(self):
        # Socket parameters
        self.socket_buffer_size = 1024
        self.socket_timeout = 15

        self.mass_1 = 'U'
        self.mass_2 = 'U'
        self.mass_3 = 'U'
        self.mass_4 = 'U'
        self.mass_5 = 'U'
        self.mass_6 = 'U'
        self.clock_quality = 'U'
        self.main_current = 'U'
        self.gps_antenna_current = 'U'
        self.dp_memory_1 = 'U'
        self.dp_memory_2 = 'U'
        self.dp_memory_3 = 'U'
        self.dp_memory_4 = 'U'
        self.initial_vco = 'U'
        self.voltage = 'U'
        self.satellites_used = 'U'
        self.system_temperature = 'U'
        self.seismometer_1_temperature = 'U'
        self.seismometer_2_temperature = 'U'
        self.time_error = 'U'
        self.uptime = 'U'

    def send_socket(self, address, port, command):
        """
        Send a socket to the Q330 and return the response.
        """
        try:
            # Definition de la socket reseau
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            # Ouverture de la socket et envoi de la requete
            sock.bind(('', 0))
            sock.settimeout(self.socket_timeout)
        except socket.error as e:
            logger.error(e)
            sys.exit(-1)

        try:
            sock.sendto(command, (address, port))
            # Reception de la reponse et fermeture de la socket
            response = sock.recv(self.socket_buffer_size)
        except socket.error as e:
            logger.error(e)
            sys.exit(-1)
        finally:
            sock.close()

        return response

    def import_data(self, address, port):
        """
        Send sockets to the Q330 and import data into this object.
        """
        # Send a status request
        # 0-11     QDP header format
        #   0-3    CRC (hash calculated on the command with the crc_calc custom
        #          utility)
        #   4      Command (\x38 for PING)
        #   5      Version (2)
        #   6-7    Length of the data (8 bytes)
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-19    Format of status request (PING)
        #   12-13  Type = 2 for a status request
        #   14-15  User message number
        #   16-19  Status request bitmap
        #          0F0B = 00001111 00101011
        status_command = (b'\x54\xBB\x5F\xF0\x38\x02\x00\x08\x00\x00\x00\x00'
                          b'\x00\x02\x00\x00\x00\x00\x0F\x2B')
        response = self.send_socket(address, port, status_command)

        # Status response
        # 0-11     QDP header format
        #   0-3    CRC
        #   4      Command
        #   5      Version
        #   6-7    Length
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-35    Format of status response
        #   0-1    Type = 3 for a status response
        #   2-3    User message number
        #   4-5    Drift tolerance
        #   6-7    User message count
        #   8-11   Time of last reboot
        #   12-19  Spare
        #   20-23  Status bitmap
        # 36-87    Global status
        #   0-1    Acquisition control
        #   2-3    Clock quality
        clock_quality_factor = unpack('>H', response[38:40])[0]
        #   4-5    Minute since lost
        minutes_since_loss = unpack('>H', response[40:42])[0]
        if clock_quality_factor & 1 == 0:
            self.clock_quality = 0
        elif clock_quality_factor & 5 == 5:
            self.clock_quality = 100
        elif clock_quality_factor & 3 == 3:
            self.clock_quality = 90
        elif clock_quality_factor & 9 == 9:
            self.clock_quality = 80
        elif (clock_quality_factor & 32 == 32) or \
                (clock_quality_factor & 16 == 16):
            self.clock_quality = 60 - minutes_since_loss / 10
            if self.clock_quality < 10:
                self.clock_quality = 10
        #   6-7    Analog voltage control value
        #   8-11   Seconds offset (32 bits)
        # From 2000/01/01 at 00:00:00, convert to epoch
        last_boot = unpack('>I', response[44:48])[0] + 946684800
        self.uptime = (time.time() - last_boot) / 86400
        #   12-15  Usec offset (32 bits)
        #   16-19  Total time in seconds
        #   20-23  Power on time in seconds
        #   24-27  Time of last re-sync
        #   28-31  Total number of re-syncs
        #   32-33  GPS status
        #   34-35  Calibrator status
        #   36-37  Sensor bitmap
        #   38-39  Current VCO
        #   40-41  Data sequence number
        #   42-43  PLL running if set
        #   44-45  Status inputs
        #   46-47  Misc. inputs
        #   48-51  Current data sequence number
        # 88-171   GPS status
        #   0-1    GPS power on/off time
        #   2-3    GPS on if <> 0
        #   4-5    Number of satellites used
        self.satellites_used = unpack('>H', response[92:94])[0]
        #   6-7    Number of satellites in view
        #   8-17   GPS time string[9]
        #   18-29  GPS date string[11]
        #   30-35  GPS fix string[5]
        #   36-47  GPS height string[11]
        #   48-61  GPS latitude string[13]
        #   62-75  GPS longitude string[13]
        #   76-79  Time of last good 1PPS
        #   80-83  Total checksum errors
        # 172-203  Booms position, temperatures and voltages
        #   0-1    Channel 1 boom
        self.mass_1 = unpack('>h', response[172:174])[0]
        #   2-3    Channel 2 boom
        self.mass_2 = unpack('>h', response[174:176])[0]
        #   4-5    Channel 3 boom
        self.mass_3 = unpack('>h', response[176:178])[0]
        #   6-7    Channel 4 boom
        self.mass_4 = unpack('>h', response[178:180])[0]
        #   8-9    Channel 5 boom
        self.mass_5 = unpack('>h', response[180:182])[0]
        #   10-11  Channel 6 boom
        self.mass_6 = unpack('>h', response[182:184])[0]
        #   12-13  Analog positive supply (10mv increments)
        #   14-15  Analog negative supply if not zero
        #   16-17  Input power (150mv increments)
        self.voltage = unpack('>h', response[188:190])[0] * 0.15
        #   18-19  System temperature (celsius)
        self.system_temperature = unpack('>h', response[190:192])[0]
        if self.system_temperature == 666:
            self.system_temperature = 'U'
        #   20-21  Main current (1ma increments)
        self.main_current = unpack('>h', response[192:194])[0]
        #   22-23  GPS antenna current (1ma increments)
        self.gps_antenna_current = unpack('>h', response[194:196])[0]
        #   24-25  Seismometer 1 temperature (celsius)
        self.seismometer_1_temperature = unpack('>h', response[196:198])[0]
        if self.seismometer_1_temperature == 666:
            self.seismometer_1_temperature = 'U'
        #   26-27  Seismometer 2 temperature (celsius)
        self.seismometer_2_temperature = unpack('>h', response[198:200])[0]
        if self.seismometer_2_temperature == 666:
            self.seismometer_2_temperature = 'U'
        #   28-31  Slave processor timeouts
        # 204-231  PLL status
        #   0-3    Initial VCO (float)
        self.initial_vco = unpack('>f', response[204:208])[0]
        #   4-7    Time error (float)
        self.time_error = unpack('>f', response[208:212])[0] * 1e9
        #   8-11   RMS VCO (float)
        #   12-15  Best VCO (float)
        #   16-19  Spare
        #   20-23  Ticks since last update
        #   24-25  Km
        #   26-27  State
        # 232-263  Data port 1 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        dp_memory_used_1 = unpack('>I', response[248:252])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags
        # 264-295  Data port 2 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        dp_memory_used_2 = unpack('>I', response[280:284])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags
        # 296-327  Data port 3 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        dp_memory_used_3 = unpack('>I', response[312:316])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags
        # 328-359  Data port 4 status
        #   0-3    Total data packets sent
        #   4-7    Total packets re-sent
        #   8-11   Total fill packets sent
        #   12-15  Receive sequence errors
        #   16-19  Bytes of packet currently used
        dp_memory_used_4 = unpack('>I', response[344:348])[0]
        #   20-23  Time of last data packet acked
        #   24-25  Physical interface number
        #   26-27  Data port number
        #   28-29  Retransmission timeout (100ms)
        #   30-31  Flags

        # Send an information request
        # 0-11     QDP header format
        #   0-3    CRC (hash calculated on the command with the crc_calc custom
        #          utility)
        #   4      Command (\x38 for PING)
        #   5      Version (2)
        #   6-7    Length of the data (4 bytes)
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-15    Format of status request (PING)
        #   12-13  Type = 4 for a information request
        #   14-15  Ignored
        information_command = (
            b'\x4D\xF9\xAA\xC0\x38\x02\x00\x04\x00\x00\x00\x00'
            b'\x00\x04\x00\x00')
        response = self.send_socket(address, port, information_command)

        # Information response
        # 0-11     QDP header format
        #   0-3    CRC
        #   4      Command
        #   5      Version
        #   6-7    Length
        #   8-9    Sequence number
        #   10-11  Acknowledge number
        # 12-83    Format of information response
        #   0-1    Type = 5 for information response
        #   2-3    Ignored
        #   4-5    Version
        #   6-7    Flags
        #   8-11   KMI property tag
        #   12-15  Serial number (high 32 bits)
        #   16-19  Serial number (low 32 bits)
        #   20-23  Data port 1 packet memory size
        dp_memory_size_1 = unpack('>I', response[32:36])[0]
        try:
            self.dp_memory_1 = (dp_memory_used_1 / dp_memory_size_1) * 100
        except ZeroDivisionError:
            self.dp_memory_1 = 0
        #   24-27  Data port 2 packet memory size
        dp_memory_size_2 = unpack('>I', response[36:40])[0]
        try:
            self.dp_memory_2 = (dp_memory_used_2 / dp_memory_size_2) * 100
        except ZeroDivisionError:
            self.dp_memory_2 = 0
        #   28-31  Data port 3 packet memory size
        dp_memory_size_3 = unpack('>I', response[40:44])[0]
        try:
            self.dp_memory_3 = (dp_memory_used_3 / dp_memory_size_3) * 100
        except ZeroDivisionError:
            self.dp_memory_3 = 0
        #   32-35  Data port 4 packet memory size
        dp_memory_size_4 = unpack('>I', response[44:48])[0]
        try:
            self.dp_memory_4 = (dp_memory_used_4 / dp_memory_size_4) * 100
        except ZeroDivisionError:
            self.dp_memory_4 = 0
        #   36-39  Serial interface 1 memory trigger level
        #   40-43  Serial interface 2 memory trigger level
        #   44-47  Reserved
        #   48-51  Ethernet interface memory trigger level
        #   52-53  Serial interface 1 advanced flags
        #   54-55  Serial interface 2 advanced flags
        #   56-57  Reserved
        #   58-59  Ethernet interface advanced flags
        #   60-61  Serial interface 1 data port number
        #   62-63  Serial interface 2 data port number
        #   64-65  Reserved
        #   66-67  Ethernet interface data port number
        #   68-69  Calibration error bitmap
        #   70-71  System software version
