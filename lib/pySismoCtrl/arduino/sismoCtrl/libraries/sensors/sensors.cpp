/* Arduino sismoCtrl library
 *
 * This library is used for control a sismoCtrl arduino shield. Plugged in
 * front of a Centaur (or Taurus), it allows control in a single script every
 * digital outputs instead of the digitizer, inject a custom input signal, and
 * is compatible with most of standard seismometer models (t120, t240, cmg40),
 * EXCEPT STS2.
 *
 */
#include "sensors.h"

//Class constructor
Sismo::Sismo(){
	//Assign pin as outputs
	pinMode(SEN_CTRL456_PIN, OUTPUT);
	pinMode(SEN_CTRL1_PIN, OUTPUT);
	pinMode(SEN_CTRL2_PIN, OUTPUT);
	pinMode(SEN_CTRL3_PIN, OUTPUT);
	//All outputs to LOW
	//Warning: makes all seismometers active low state in sp, uvw and
	//injection mode!!
	//Impacts following models: cmg40_low
	digitalWrite(SEN_CTRL456_PIN, LOW);
	digitalWrite(SEN_CTRL1_PIN, LOW);
	digitalWrite(SEN_CTRL2_PIN, LOW);
	digitalWrite(SEN_CTRL3_PIN, LOW);
}

void Sismo::cal(bool state){	
	//SEN_CTRL456_PIN active, custom signal on BNC connector injected. 
	//See shield schematic for details.
	digitalWrite(SEN_CTRL456_PIN, state);
}

void Sismo::ctrl1(bool state){	
	//SEN_CTRL456_PIN active, custom signal on BNC connector injected. 
	//See shield schematic for details.
	digitalWrite(SEN_CTRL1_PIN, state);
}

void Sismo::ctrl2(bool state){	
	//SEN_CTRL456_PIN active, custom signal on BNC connector injected. 
	//See shield schematic for details.
	digitalWrite(SEN_CTRL2_PIN, state);
}

void Sismo::ctrl3(bool state){	
	//SEN_CTRL456_PIN active, custom signal on BNC connector injected. 
	//See shield schematic for details.
	digitalWrite(SEN_CTRL3_PIN, state);
}

void Sismo::close(void){
	//All outputs to LOW
	//Warning: makes all seismometers active low state in sp, uvw and
	//injection mode!!
	//Impacts following models: cmg40_low
	digitalWrite(SEN_CTRL456_PIN, LOW);
	digitalWrite(SEN_CTRL1_PIN, LOW);
	digitalWrite(SEN_CTRL2_PIN, LOW);
	digitalWrite(SEN_CTRL3_PIN, LOW);
}
	
