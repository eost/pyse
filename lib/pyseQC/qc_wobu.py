#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np
from obspy.core import Trace, UTCDateTime
from scipy.signal import freqs_zpk, filtfilt
import soundfile as sf

from coilcalib import H_fft_div
from pyse_util import extract_paz_from_yamlcatalog


def _qc_wobu(wob_requests, server, conf, colors):

    if len(wob_requests) > 0:
        amplitudes = list()
        phases = list()
        amp_errors = list()
        pha_errors = list()
        paz = extract_paz_from_yamlcatalog(conf['qc_wobu']
                                               ['combined_resp_file'],
                                           conf['main']['sensor_model'])

        i = 0
        theory = False
        for request in wob_requests:
            print("%d %s" % (i//3, request))
            i += 1
            words = request.split(' ')
            t1 = UTCDateTime(words[2])
            t2 = t1 + 600
            seed_codes = words[4].replace(words[4][-3:-1], 'HH').split('.')
            try:
                tr = server.get_waveforms(seed_codes[0], seed_codes[1],
                                          seed_codes[2], seed_codes[3], t1,
                                          t2)[0]
                tr.trim(t1, t2-tr.stats.delta)
                if words[0] == 'wob_analyzer_centaur':
                    tr_ref = Trace(header={'network': 'XX', 'station': 'REF',
                                           'location': '00', 'channel': 'HC'})
                    data, sr = sf.read(conf['qc_wobu']['sound_file'],
                                       format='RAW', subtype='PCM_16',
                                       channels=1, samplerate=30000,
                                       endian='little')
                    tr_ref.data = data
                    tr_ref.stats.sampling_rate = sr

                    seed_id = conf['qc_wobu']['ref_resp']
                    code_ref = seed_id.split('.')
                    inv_ref = server.get_stations(network=code_ref[0],
                                                  station=code_ref[1],
                                                  location=code_ref[2],
                                                  channel=code_ref[3],
                                                  starttime=t1, endtime=t2,
                                                  level='response')
                    resp_ref = inv_ref.get_response(seed_id, t1)

                    for stage in resp_ref.response_stages:
                        if stage.input_units.lower()[:5] == 'count' and\
                           stage.decimation_factor != 1:
                            coeff_array = np.array(stage.coefficients)
                            if stage.symmetry == 'ODD':
                                coeff_array = np.append(coeff_array,
                                                        coeff_array[-2::-1])
                            elif stage.symmetry == 'EVEN':
                                coeff_array = np.append(coeff_array,
                                                        coeff_array[-1::-1])
                            tr_ref.data = filtfilt(coeff_array, 1, tr_ref.data,
                                                   padtype=None)
                            tr_ref.decimate(stage.decimation_factor,
                                            no_filter=True)

                elif words[0] == 'wob_analyzer':
                    for word in words:
                        if word[:12] == '--ref-stream':
                            ref_stream = word.split('=')[1]
                    if not ref_stream:
                        print("%s: no reference stream" % (request))
                    else:
                        seed_codes =\
                            ref_stream.replace('BH', 'HH').split('.')
                        tr_ref = server.get_waveforms(seed_codes[0],
                                                      seed_codes[1],
                                                      seed_codes[2],
                                                      seed_codes[3], t1, t2)[0]
                        tr_ref.trim(t1, t2-tr.stats.delta)

                (f, A, P) = H_fft_div(tr_ref, tr,
                                      sm_n=conf['qc_wobu']['smooth'],
                                      fnorm=paz['normalization_frequency'],
                                      normalize=True)

                if theory is False:
                    _w, H0 = freqs_zpk(paz['zeros'], paz['poles'], paz['gain'],
                                       worN=2*np.pi*f)
                    Amp_th = np.absolute(H0)
                    Pha_th = np.angle(H0, deg=True)
                    theory = True

                imin = np.abs(f-1).argmin()
                imax = np.abs(f-80).argmin()
                amp_err = np.abs(20*np.log10(A[imin:imax]/Amp_th[imin:imax]))
                amp_err = amp_err.max()
                pha_err = np.abs(P[imin:imax]-Pha_th[imin:imax]).max()
                if amp_err > 10 or pha_err > 30:
                    print("Result out of range")
                else:
                    amplitudes.append(A)
                    phases.append(P)
                    amp_errors.append(amp_err)
                    pha_errors.append(pha_err)

            except Exception as e:
                print(e)

        if len(amplitudes) == len(phases) and\
           len(amplitudes) == len(amp_errors) and\
           len(amplitudes) == len(pha_errors) and\
           len(amplitudes) > 0:

            amplitudes = np.array(amplitudes)
            phases = np.array(phases)
            amp_errors = np.array(amp_errors)
            pha_errors = np.array(pha_errors)

            np.savez_compressed("%s" % conf['qc_wobu']['npzfile'],
                                frequencies=f,
                                amplitudes=[np.percentile(amplitudes, 25,
                                                          axis=0),
                                            np.median(amplitudes, axis=0),
                                            np.percentile(amplitudes, 75,
                                                          axis=0)],
                                phases=[np.percentile(phases, 25, axis=0),
                                        np.median(phases, axis=0),
                                        np.percentile(phases, 75, axis=0)])

            print("Wobu statistics:")
            print("Amplitude maximal error from theoretical response: %.2f dB"
                  % amp_errors.max())
            print("Amplitude median error from theoretical response: %.2f dB"
                  % np.median(amp_errors))
            print("Phase maximal error from theoretical response: %.2f deg"
                  % pha_errors.max())
            print("Phase median error from theoretical response: %.2f deg"
                  % np.median(pha_errors))
            print("")

            fw = pyplot.figure('wobu')
            ax1 = fw.add_subplot(211)
            ax2 = fw.add_subplot(212)
            for i in range(len(amplitudes)):
                ax1.semilogx(f, 20*np.log10(amplitudes[i]),
                             color=colors[i], zorder=1)
                ax2.semilogx(f, phases[i], color=colors[i], zorder=1)
            ax1.semilogx(f, 20*np.log10(np.median(amplitudes, axis=0)),
                         '-k', label='median', zorder=2)
            ax1.semilogx(f, 20*np.log10(np.percentile(amplitudes, 25, axis=0)),
                         ':k', label='quartiles', zorder=2)
            ax1.semilogx(f, 20*np.log10(np.percentile(amplitudes, 75, axis=0)),
                         ':k', zorder=2)
            ax1.semilogx(f, 20*np.log10(Amp_th), '--k',
                         label='theory', zorder=2)
            ax2.semilogx(f, np.median(phases, axis=0), '-k', zorder=2)
            ax2.semilogx(f, np.percentile(phases, 25, axis=0), ':k', zorder=2)
            ax2.semilogx(f, np.percentile(phases, 75, axis=0), ':k', zorder=2)
            ax2.semilogx(f, Pha_th, '--k', zorder=2)
            [ax.grid() for ax in [ax1, ax2]]
            [ax.set_xlim((10, 90)) for ax in [ax1, ax2]]
            ax1.set_ylim((-15, 0))
            ax2.set_ylim((-180, 0))
            ax1.set_ylabel("Norm. Amplitude [dB][V.s/m]")
            ax2.set_ylabel("Phase [deg]")
            ax2.set_xlabel("Frequency [Hz]")
            ax1.legend(loc='best')
            ax1.set_xticklabels([])
            ax1.set_xticklabels([], minor=True)

            fw.savefig(conf['qc_wobu']['epsfile'])
