#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
:author:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:copyright:
    Maxime Bès de Berc (mbesdeberc@unistra.fr)
:license:
    The Beerware License
    (https://tldrlegal.com/license/beerware-license)
"""
from matplotlib import pyplot
import numpy as np
from obspy.core import UTCDateTime
import yaml

from repimp import repimp
from rotation import xyz2uvw


def _qc_repimp(repimp_requests, server, conf, colors):

    if len(repimp_requests) > 0:
        periods = np.array([])
        periods_std = np.array([])
        dampings = np.array([])
        dampings_std = np.array([])
        matrix = None
        error = conf['main']['error']

        i = 0
        for request in repimp_requests:
            print("%d %s" % (i, request))
            i += 1
            words = request.split(' ')
            t1 = UTCDateTime(words[2])
            t2 = UTCDateTime(words[3])
            stream = words[4].replace(words[4][-3:-1],
                                      conf['qc_repimp']['stream'])
            seed_codes = stream.split('.')

            for w in words:
                if w[:8] == '--rotate':
                    matrix = w.split('=')[-1]
            try:
                wf = server.get_waveforms(seed_codes[0], seed_codes[1],
                                          seed_codes[2], seed_codes[3], t1, t2)

                wf.trim(t1, t2)

                if matrix is not None:
                    st0 = wf.copy()
                    mat_yaml = open(conf['qc_repimp']['rot_matrix_file'])
                    mat = yaml.load(mat_yaml)
                    mat_yaml.close()
                    if matrix == 't240':
                        st0 = xyz2uvw(wf, matrix=np.array(mat[matrix]))
                    elif matrix == 't120':
                        st0 = xyz2uvw(wf, matrix=np.array(mat[matrix]))
                    elif matrix == 'sts2':
                        st0 = xyz2uvw(wf, matrix=np.array(mat[matrix]))
                    wf = st0.copy()

                for tr in wf:
                    (P, Ps, D, Ds, SNR) = repimp(tr, verbose=False,
                                                 plotting=False,
                                                 synchro_coils=True,
                                                 period=conf['main']
                                                 ['sensor_period'])
                    if P > conf['main']['sensor_period']*(1+error) or\
                       P < conf['main']['sensor_period']*(1-error) or\
                       D > conf['main']['sensor_damping']*(1+error) or\
                       D < conf['main']['sensor_damping']*(1-error) or\
                       SNR < conf['qc_repimp']['snr']:
                        print("Result out of range")
                    else:
                        periods = np.append(periods, P)
                        periods_std = np.append(periods_std, Ps)
                        dampings = np.append(dampings, D)
                        dampings_std = np.append(dampings_std, Ds)
            except Exception as e:
                print(e)

        max_error_thperiod = np.abs(1 - periods /
                                    conf['main']['sensor_period']).max()*100
        max_error_medperiod = np.abs(1 - periods/np.median(periods)).max()*100
        max_error_thdamping = np.abs(1 - dampings /
                                     conf['main']['sensor_damping']).max()*100
        max_error_meddamping = np.abs(1 - dampings /
                                      np.median(dampings)).max()*100

        if len(periods) == len(periods_std) and\
           len(periods) == len(dampings) and\
           len(periods) == len(dampings_std) and len(periods) > 0:

            np.savez_compressed(conf['qc_repimp']['npzfile'],
                                periods=periods, dampings=dampings)

            per_interquartile = np.abs(np.percentile(periods, 75) -
                                       np.percentile(periods, 25))
            damp_interquartile = np.abs(np.percentile(dampings, 75) -
                                        np.percentile(dampings, 25))
            print("Repimp statistics:")
            print("Median period: %.3f seconds" % (np.median(periods)))
            print("  Interquartile range: %.3f seconds" % per_interquartile)
            print("Median damping: %.6f" % (np.median(dampings)))
            print("  Interquartile range: %.6f" % damp_interquartile)
            print("Max error from theoretical period: %.3f percent" %
                  (max_error_thperiod))
            print("Max error from median period: %.3f percent" %
                  (max_error_medperiod))
            print("Max error from theoretical damping: %.3f percent" %
                  (max_error_thdamping))
            print("Max error from median damping: %.3f percent" %
                  (max_error_meddamping))
            print("")

            fr = pyplot.figure('repimp', dpi=150)
            ax = fr.gca()
            ax.plot(periods, dampings, 'o', color=colors[1], zorder=1)
            ax.plot(conf['main']['sensor_period'],
                    conf['main']['sensor_damping'], 'v',
                    color=colors[0], label='theory', zorder=2)
            ax.errorbar(np.median(periods), np.median(dampings),
                        xerr=per_interquartile/2, yerr=damp_interquartile/2,
                        fmt='o', color=colors[0], ecolor=colors[0], zorder=2,
                        label='median/quartiles')
            ax.set_xlabel("Periods (s)")
            ax.set_ylabel("Dampings")
            ax.grid()
            ax.legend(loc='lower right')

            fr.savefig(conf['qc_repimp']['epsfile'])
